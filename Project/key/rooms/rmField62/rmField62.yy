
{
    "name": "rmField62",
    "id": "685b7881-755f-4885-a92c-56942c3ffe34",
    "creationCodeFile": "",
    "inheritCode": true,
    "inheritCreationOrder": false,
    "inheritLayers": true,
    "instanceCreationOrderIDs": [
        "db67a2f6-3d8c-47e0-a0f5-0b7bc3c75011",
        "61d9bb29-3044-4a21-bea5-54646d24ff3c",
        "643d8686-3ef7-4b7f-a967-4a7335cddb2e",
        "043195fb-b34e-452a-867b-fecab57da282",
        "b2750c9c-5656-4bdc-b316-b05c2aa7fcea",
        "dcc73334-fae8-42fb-afe4-bd64da47b37b",
        "3c2bcd19-f26c-4781-a520-827e00fd0519",
        "80d10036-ef2f-4748-ace8-875b5546957e",
        "d5436c61-81c2-4a11-add6-05579221fdc4",
        "d2f5ae2c-1156-443d-be3a-2da637bc8d86",
        "e1654ae6-1232-4940-b1e9-2c3a2d23e4a9",
        "1a8fc18e-6812-41cc-8490-3f3dde4ee12d",
        "5078a30d-54cf-40db-ae10-ec1c6b96e5ed",
        "aed60a83-ba83-4b8a-845e-33972d00c1e4",
        "e25dd714-0bc6-4b78-a9fb-2b0e4028c716",
        "41019dea-fc89-4c9d-a274-4a031e95f9bb",
        "299680b3-fa7d-45f4-aff6-ab8b1a79eeb9",
        "a2bfb541-b5d2-45f5-8534-69345ffc65fc",
        "3c0d782e-4005-46bd-b062-1fb369fb5335",
        "01025acd-b495-4c32-a081-d541224a6e39",
        "50017ec3-b871-4c85-9c29-62fcc4a23358",
        "d5eb4a85-4cc5-4758-bf20-f1ccabe72a6b",
        "9b0ea121-974c-4276-9879-941eb9467ca6",
        "b6ec72e1-e47c-40fb-a500-9babe9af8969",
        "6d708e47-afbe-43f6-b7ae-75c893722851"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "a0d4bb35-c16a-4c49-b7a4-1522f98f567c",
            "depth": 0,
            "grid_x": 16,
            "grid_y": 16,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": true,
            "inheritLayerSettings": false,
            "inheritSubLayers": true,
            "inheritVisibility": true,
            "instances": [
{"name": "inst_54F329EE","id": "db67a2f6-3d8c-47e0-a0f5-0b7bc3c75011","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "cf531036-120e-4a42-8b37-d1822e7c2069","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_54F329EE","objId": "bf6bb1ba-440c-4cd6-896f-488204332c85","properties": null,"rotation": 0,"scaleX": 20,"scaleY": 1,"mvc": "1.1","x": 160,"y": 8},
{"name": "inst_6DC6073D","id": "61d9bb29-3044-4a21-bea5-54646d24ff3c","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "8d60b46c-6f25-417e-8d2a-579d32f9a87f","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6DC6073D","objId": "bf6bb1ba-440c-4cd6-896f-488204332c85","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 8,"mvc": "1.1","x": 8,"y": 80},
{"name": "inst_463C5540","id": "643d8686-3ef7-4b7f-a967-4a7335cddb2e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "7d9bd6ad-6b37-4013-9347-f16b4970a44b","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_463C5540","objId": "bf6bb1ba-440c-4cd6-896f-488204332c85","properties": null,"rotation": 0,"scaleX": 20,"scaleY": 1,"mvc": "1.1","x": 160,"y": 152},
{"name": "inst_2034B6FF","id": "043195fb-b34e-452a-867b-fecab57da282","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "59c00608-9477-43bb-9c60-0a360b57aaec","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2034B6FF","objId": "bf6bb1ba-440c-4cd6-896f-488204332c85","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 8,"mvc": "1.1","x": 312,"y": 80},
{"name": "inst_70FCE6FD","id": "b2750c9c-5656-4bdc-b316-b05c2aa7fcea","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "36c63420-7936-4877-9977-a963d24018d9","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_70FCE6FD","objId": "bf6bb1ba-440c-4cd6-896f-488204332c85","properties": null,"rotation": 0,"scaleX": 12,"scaleY": 1,"mvc": "1.1","x": 160,"y": 88},
{"name": "inst_3A34E231","id": "dcc73334-fae8-42fb-afe4-bd64da47b37b","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "c9146e2e-5e06-46b0-a494-cd0f92b20954","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3A34E231","objId": "a88c679d-d029-40d9-bbf6-d22448fe97d4","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 88,"y": 72},
{"name": "inst_3A8D09FA","id": "3c2bcd19-f26c-4781-a520-827e00fd0519","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_3A8D09FA.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "be775e19-c77d-40c7-959e-649f6ad29d93","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3A8D09FA","objId": "ade19e8b-e1e1-43b6-baa8-5bfd1a76960a","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 232,"y": 72},
{"name": "inst_1FB2F13A","id": "80d10036-ef2f-4748-ace8-875b5546957e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "393ecd88-5123-42af-9846-12c3f96f839d","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1FB2F13A","objId": "1efc200f-1458-4af7-ad13-39cc75d86d33","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 280,"y": 136},
{"name": "inst_9C5F18F","id": "d5436c61-81c2-4a11-add6-05579221fdc4","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_9C5F18F.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "77b4b76e-0b0d-4220-a0fa-b9b521c6ddb9","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_9C5F18F","objId": "ebf718d0-9020-4452-8d60-f129d6dca74d","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 40,"y": 136},
{"name": "inst_91AB893","id": "d2f5ae2c-1156-443d-be3a-2da637bc8d86","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_91AB893.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "0583844e-c251-49ae-b7ff-992d55bb00a1","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_91AB893","objId": "bf6bb1ba-440c-4cd6-896f-488204332c85","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 280,"y": 136},
{"name": "inst_72A291D9","id": "e1654ae6-1232-4940-b1e9-2c3a2d23e4a9","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "24af1984-f2ca-46d5-985d-231a6e390b6b","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_72A291D9","objId": "2c526aaa-d882-4b28-96e4-ec8a99716676","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 72,"y": 104},
{"name": "inst_770D8D5F","id": "1a8fc18e-6812-41cc-8490-3f3dde4ee12d","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "6207d307-d36c-4497-ac43-5520bd55423e","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_770D8D5F","objId": "2c526aaa-d882-4b28-96e4-ec8a99716676","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 72,"y": 136},
{"name": "inst_76106ABD","id": "5078a30d-54cf-40db-ae10-ec1c6b96e5ed","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "c8e613d6-4bc0-4314-abac-66747ac2d53f","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_76106ABD","objId": "2c526aaa-d882-4b28-96e4-ec8a99716676","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 120,"y": 136},
{"name": "inst_469A8D4C","id": "aed60a83-ba83-4b8a-845e-33972d00c1e4","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "431a782b-eeed-41b8-b8bb-3ee23f013759","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_469A8D4C","objId": "2c526aaa-d882-4b28-96e4-ec8a99716676","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 120,"y": 120},
{"name": "inst_74890B8D","id": "e25dd714-0bc6-4b78-a9fb-2b0e4028c716","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "655b64ed-fd8c-4bc7-ba93-14dc316e62b2","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_74890B8D","objId": "2c526aaa-d882-4b28-96e4-ec8a99716676","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 248,"y": 104},
{"name": "inst_58407B15","id": "41019dea-fc89-4c9d-a274-4a031e95f9bb","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "a4b827d4-f7e3-4285-93da-15242b88cac7","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_58407B15","objId": "2c526aaa-d882-4b28-96e4-ec8a99716676","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 248,"y": 136},
{"name": "inst_7DDBEF2A","id": "299680b3-fa7d-45f4-aff6-ab8b1a79eeb9","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "019fed13-60e2-476e-ac52-1315d763d6f0","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7DDBEF2A","objId": "2c526aaa-d882-4b28-96e4-ec8a99716676","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 200,"y": 136},
{"name": "inst_61F73ADD","id": "a2bfb541-b5d2-45f5-8534-69345ffc65fc","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "ab8e0f17-be4d-4c8a-a4b4-eb3e756fe894","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_61F73ADD","objId": "2c526aaa-d882-4b28-96e4-ec8a99716676","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 200,"y": 120},
{"name": "inst_41C424DD","id": "3c0d782e-4005-46bd-b062-1fb369fb5335","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "7a252196-8321-43e4-897b-ab9a75b169a0","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_41C424DD","objId": "2c526aaa-d882-4b28-96e4-ec8a99716676","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 152,"y": 104},
{"name": "inst_E50C397","id": "01025acd-b495-4c32-a081-d541224a6e39","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "5ccc0d3b-9727-4a0f-9f92-42d514b733ce","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_E50C397","objId": "2c526aaa-d882-4b28-96e4-ec8a99716676","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 168,"y": 104},
{"name": "inst_656D4FD7","id": "50017ec3-b871-4c85-9c29-62fcc4a23358","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "327ac987-a1dd-4793-96a2-a2503b2071c8","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_656D4FD7","objId": "2c526aaa-d882-4b28-96e4-ec8a99716676","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 136,"y": 136},
{"name": "inst_44C12B7E","id": "d5eb4a85-4cc5-4758-bf20-f1ccabe72a6b","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "c491e711-74b8-4ae3-a822-9d7085c72e71","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_44C12B7E","objId": "2c526aaa-d882-4b28-96e4-ec8a99716676","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 88,"y": 104},
{"name": "inst_F79F37B","id": "9b0ea121-974c-4276-9879-941eb9467ca6","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "f7947d1c-2f81-4217-91f1-85c7a5854f08","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_F79F37B","objId": "2c526aaa-d882-4b28-96e4-ec8a99716676","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 232,"y": 104},
{"name": "inst_516475","id": "b6ec72e1-e47c-40fb-a500-9babe9af8969","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "74aeff2f-3e7e-4c15-89e1-12a0b5691dfc","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_516475","objId": "2c526aaa-d882-4b28-96e4-ec8a99716676","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 184,"y": 136},
{"name": "inst_36CF8AB1","id": "6d708e47-afbe-43f6-b7ae-75c893722851","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "9349200f-8cc1-4348-b547-4325dab3eb0e","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_36CF8AB1","objId": "4e250d44-df5e-4bf9-9d9e-ddeed4940a8c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 160,"y": 128}
            ],
            "layers": [

            ],
            "m_parentID": "62e4cb23-ce44-4e39-844e-172822e70c0e",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "d90f8113-0cf1-4928-a0c8-aa72c95cadb6",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4294967295 },
            "depth": 100,
            "grid_x": 16,
            "grid_y": 16,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": true,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": true,
            "layers": [

            ],
            "m_parentID": "3ef1a5d1-b164-423f-9134-f545d7e2aed9",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "e429060d-a0c1-49f9-b73e-50deeeccbf00",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": true,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "7372a4e7-4518-4b99-94c5-db38cab10389",
    "physicsSettings":     {
        "id": "39f8f1a7-b8e8-4224-aa07-5ad241971b91",
        "inheritPhysicsSettings": true,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "caaeb510-c79d-4233-bb1c-f46a2e550a7f",
        "Height": 180,
        "inheritRoomSettings": true,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 320
    },
    "mvc": "1.0",
    "views": [
{"id": "56935aab-dda0-441f-a1a5-49c063dd0d20","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "e9c50517-5bb7-450c-a9e2-ee8ab76127cf","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "38a1de43-edf0-4f9a-8c60-663b0810b732","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "fae29e62-001f-4eab-9762-baf1f782b1fa","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "7c4a1a28-1c33-45e1-a39e-1ba5f4f48769","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "8ee0deec-def2-464a-b247-00fafba6512a","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "e8ce98ba-dea9-4863-a73d-bcc44b2904fa","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "93a144e3-7727-4bac-9ce2-a359a53f989e","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "c3ff5bc8-42b9-41cc-9df2-ad755ccac631",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": false,
        "inheritViewSettings": true,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}