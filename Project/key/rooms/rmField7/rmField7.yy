
{
    "name": "rmField7",
    "id": "7b502cdd-7541-4c17-8143-78b84912f517",
    "creationCodeFile": "",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "3cb8c4dc-03b1-42a0-94ee-b0583349ce3d",
        "d5508125-4464-40df-a970-cbc7a97f593f",
        "ef91ce6b-7e0e-441f-9969-c637b41d385a",
        "241f5600-c779-4d2c-a489-306be4ed12d3",
        "e627234b-646a-42c9-9b23-bf287af7af3e",
        "c8607f3c-0bf0-4586-a016-49394fd0a520",
        "d08215f9-b000-4ec2-903d-ca8dd8093c38",
        "d1e2a21b-00aa-4921-b874-18920eadcff0",
        "f7b14472-391a-42b3-91c8-7f0fcbcb9a90",
        "1b724d26-835b-4001-b60c-d4e084d9e291",
        "2734ac4e-d9b1-4db7-ae3c-2bd29e148fca",
        "4a58ad8a-4675-44d5-8d71-8ac3a3ff3a21",
        "1d53e7b6-89ee-45db-8887-5505045f28ee",
        "3ebaf7f8-126a-42ae-96d7-e3e039b72273",
        "95157f11-17a2-46e8-ab96-33a2300ce54c",
        "a6caeb5a-95e3-4f97-90b3-d81ab19dc7d1",
        "0c12a208-442b-497b-8174-e002956fbbda",
        "c7f12c95-bb93-495d-9a34-f2ce9a1a881e",
        "cb758a3a-aa9f-41e5-b227-b705099bc67a",
        "b559f79e-0511-480c-9c1b-35b477a9795e",
        "91309ee0-89c4-4f05-9e80-e3173593d1db",
        "65ce855b-6e15-4cab-9738-7f3c3680ef33",
        "51aaf6d9-bd59-4bf5-88a3-4cdbbb2db79a",
        "da23de88-b261-43e2-bb38-6c6b9ddf5c62",
        "e136570d-be95-4ef8-aac6-0a5828e0a1a6",
        "735c74ee-ac3e-458b-b265-30855c602781",
        "b1373b72-6c50-4b68-8959-bc068ffaa6e9",
        "93c1bfc2-3f6b-4cf5-9717-0a6edf4b7993",
        "9479feb7-41c8-4036-883b-3a52aacfe903",
        "9f471a8f-4a43-4bb7-a0b4-9fc9997b368a",
        "2f816160-2723-4667-bd53-5245aeaa985a",
        "502ffa42-65b3-4c57-8930-3be4bf91ee9f",
        "13c5e61d-9998-4c26-9910-11149294a3fc",
        "8bb2eb54-d422-4297-969c-4398ebea203a",
        "d18d0da9-343d-4542-93e0-fbc722167651",
        "57ffb104-ac08-4b73-b0ac-225213439094",
        "4686f092-3a26-4a0e-8680-583fc8630577",
        "364a0400-bdcd-4afb-a509-e5e2a17d83e6",
        "e87509e3-a386-44d9-8fab-7b38dba0524d",
        "a3da62c6-439f-4957-b87e-ad9efc657f39",
        "001d1d71-cb23-4976-a644-a83d86708d4c"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "13c8d10b-5d2a-46e9-90e8-092da90bcccf",
            "depth": 0,
            "grid_x": 8,
            "grid_y": 8,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_14745029","id": "3cb8c4dc-03b1-42a0-94ee-b0583349ce3d","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_14745029","objId": "82ba5719-c9fa-4a0d-a577-84fca46d89b7","properties": null,"rotation": 0,"scaleX": 20,"scaleY": 1,"mvc": "1.1","x": 160,"y": 152},
{"name": "inst_3C71FC1F","id": "d5508125-4464-40df-a970-cbc7a97f593f","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3C71FC1F","objId": "82ba5719-c9fa-4a0d-a577-84fca46d89b7","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 9,"mvc": "1.1","x": 8,"y": 72},
{"name": "inst_5F006C2D","id": "ef91ce6b-7e0e-441f-9969-c637b41d385a","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5F006C2D","objId": "82ba5719-c9fa-4a0d-a577-84fca46d89b7","properties": null,"rotation": 0,"scaleX": 19,"scaleY": 1,"mvc": "1.1","x": 168,"y": 8},
{"name": "inst_78CC85C5","id": "241f5600-c779-4d2c-a489-306be4ed12d3","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_78CC85C5","objId": "82ba5719-c9fa-4a0d-a577-84fca46d89b7","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 8,"mvc": "1.1","x": 312,"y": 80},
{"name": "inst_181D92FD","id": "e627234b-646a-42c9-9b23-bf287af7af3e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_181D92FD","objId": "82ba5719-c9fa-4a0d-a577-84fca46d89b7","properties": null,"rotation": 0,"scaleX": 5,"scaleY": 5,"mvc": "1.1","x": 232,"y": 88},
{"name": "inst_57D7817C","id": "c8607f3c-0bf0-4586-a016-49394fd0a520","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_57D7817C","objId": "82ba5719-c9fa-4a0d-a577-84fca46d89b7","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 4,"mvc": "1.1","x": 296,"y": 80},
{"name": "inst_437C227D","id": "d08215f9-b000-4ec2-903d-ca8dd8093c38","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_437C227D.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_437C227D","objId": "ade19e8b-e1e1-43b6-baa8-5bfd1a76960a","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 280,"y": 40},
{"name": "inst_D02B7E3","id": "d1e2a21b-00aa-4921-b874-18920eadcff0","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_D02B7E3","objId": "1efc200f-1458-4af7-ad13-39cc75d86d33","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 296,"y": 136},
{"name": "inst_451B2417","id": "f7b14472-391a-42b3-91c8-7f0fcbcb9a90","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_451B2417","objId": "b5af6e82-736e-4738-91d1-b1fc6dba708e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 184,"y": 56},
{"name": "inst_5D85116C","id": "1b724d26-835b-4001-b60c-d4e084d9e291","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5D85116C","objId": "b5af6e82-736e-4738-91d1-b1fc6dba708e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 168,"y": 72},
{"name": "inst_240A183A","id": "2734ac4e-d9b1-4db7-ae3c-2bd29e148fca","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_240A183A","objId": "b5af6e82-736e-4738-91d1-b1fc6dba708e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 152,"y": 88},
{"name": "inst_7B16231F","id": "4a58ad8a-4675-44d5-8d71-8ac3a3ff3a21","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7B16231F","objId": "b5af6e82-736e-4738-91d1-b1fc6dba708e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 136,"y": 104},
{"name": "inst_5735EC27","id": "1d53e7b6-89ee-45db-8887-5505045f28ee","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5735EC27","objId": "b5af6e82-736e-4738-91d1-b1fc6dba708e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 120,"y": 120},
{"name": "inst_434C9AF6","id": "3ebaf7f8-126a-42ae-96d7-e3e039b72273","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_434C9AF6","objId": "b5af6e82-736e-4738-91d1-b1fc6dba708e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 104,"y": 136},
{"name": "inst_58D4185","id": "95157f11-17a2-46e8-ab96-33a2300ce54c","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_58D4185","objId": "b5af6e82-736e-4738-91d1-b1fc6dba708e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 136,"y": 120},
{"name": "inst_63F18FC2","id": "a6caeb5a-95e3-4f97-90b3-d81ab19dc7d1","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_63F18FC2","objId": "b5af6e82-736e-4738-91d1-b1fc6dba708e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 136,"y": 136},
{"name": "inst_C9D5ABC","id": "0c12a208-442b-497b-8174-e002956fbbda","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_C9D5ABC","objId": "b5af6e82-736e-4738-91d1-b1fc6dba708e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 168,"y": 88},
{"name": "inst_65D66D65","id": "c7f12c95-bb93-495d-9a34-f2ce9a1a881e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_65D66D65","objId": "b5af6e82-736e-4738-91d1-b1fc6dba708e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 168,"y": 104},
{"name": "inst_166D2B8A","id": "cb758a3a-aa9f-41e5-b227-b705099bc67a","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_166D2B8A","objId": "b5af6e82-736e-4738-91d1-b1fc6dba708e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 168,"y": 120},
{"name": "inst_481775AB","id": "b559f79e-0511-480c-9c1b-35b477a9795e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_481775AB","objId": "b5af6e82-736e-4738-91d1-b1fc6dba708e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 168,"y": 136},
{"name": "inst_404C788F","id": "91309ee0-89c4-4f05-9e80-e3173593d1db","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_404C788F","objId": "b5af6e82-736e-4738-91d1-b1fc6dba708e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 184,"y": 72},
{"name": "inst_795D81F9","id": "65ce855b-6e15-4cab-9738-7f3c3680ef33","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_795D81F9","objId": "b5af6e82-736e-4738-91d1-b1fc6dba708e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 152,"y": 104},
{"name": "inst_4AD7B46F","id": "51aaf6d9-bd59-4bf5-88a3-4cdbbb2db79a","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4AD7B46F","objId": "b5af6e82-736e-4738-91d1-b1fc6dba708e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 120,"y": 136},
{"name": "inst_4CCE508C","id": "da23de88-b261-43e2-bb38-6c6b9ddf5c62","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4CCE508C","objId": "b5af6e82-736e-4738-91d1-b1fc6dba708e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 184,"y": 88},
{"name": "inst_77E96F77","id": "e136570d-be95-4ef8-aac6-0a5828e0a1a6","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_77E96F77","objId": "b5af6e82-736e-4738-91d1-b1fc6dba708e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 184,"y": 104},
{"name": "inst_49929133","id": "735c74ee-ac3e-458b-b265-30855c602781","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_49929133","objId": "b5af6e82-736e-4738-91d1-b1fc6dba708e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 184,"y": 120},
{"name": "inst_25845775","id": "b1373b72-6c50-4b68-8959-bc068ffaa6e9","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_25845775","objId": "b5af6e82-736e-4738-91d1-b1fc6dba708e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 184,"y": 136},
{"name": "inst_1B8A1AC9","id": "93c1bfc2-3f6b-4cf5-9717-0a6edf4b7993","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1B8A1AC9","objId": "b5af6e82-736e-4738-91d1-b1fc6dba708e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 152,"y": 120},
{"name": "inst_2231FD53","id": "9479feb7-41c8-4036-883b-3a52aacfe903","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2231FD53","objId": "b5af6e82-736e-4738-91d1-b1fc6dba708e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 152,"y": 136},
{"name": "inst_3BE800BD","id": "9f471a8f-4a43-4bb7-a0b4-9fc9997b368a","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3BE800BD","objId": "a88c679d-d029-40d9-bbf6-d22448fe97d4","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 56,"y": 136},
{"name": "inst_48CDABFB","id": "2f816160-2723-4667-bd53-5245aeaa985a","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_48CDABFB","objId": "82ba5719-c9fa-4a0d-a577-84fca46d89b7","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 24,"y": 24},
{"name": "inst_6D452B40","id": "502ffa42-65b3-4c57-8930-3be4bf91ee9f","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6D452B40","objId": "82ba5719-c9fa-4a0d-a577-84fca46d89b7","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 24,"y": 40},
{"name": "inst_7C11AA1","id": "13c5e61d-9998-4c26-9910-11149294a3fc","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7C11AA1","objId": "82ba5719-c9fa-4a0d-a577-84fca46d89b7","properties": null,"rotation": 0,"scaleX": 4,"scaleY": 1,"mvc": "1.1","x": 48,"y": 56},
{"name": "inst_7213A6EE","id": "8bb2eb54-d422-4297-969c-4398ebea203a","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7213A6EE","objId": "82ba5719-c9fa-4a0d-a577-84fca46d89b7","properties": null,"rotation": 0,"scaleX": 3,"scaleY": 1,"mvc": "1.1","x": 40,"y": 72},
{"name": "inst_A5462DE","id": "d18d0da9-343d-4542-93e0-fbc722167651","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_A5462DE","objId": "82ba5719-c9fa-4a0d-a577-84fca46d89b7","properties": null,"rotation": 0,"scaleX": 2,"scaleY": 1,"mvc": "1.1","x": 32,"y": 88},
{"name": "inst_6A395D65","id": "57ffb104-ac08-4b73-b0ac-225213439094","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6A395D65","objId": "82ba5719-c9fa-4a0d-a577-84fca46d89b7","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 24,"y": 104},
{"name": "inst_2ABE4149","id": "4686f092-3a26-4a0e-8680-583fc8630577","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2ABE4149","objId": "82ba5719-c9fa-4a0d-a577-84fca46d89b7","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 296,"y": 120},
{"name": "inst_385CFF02","id": "364a0400-bdcd-4afb-a509-e5e2a17d83e6","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_385CFF02","objId": "82ba5719-c9fa-4a0d-a577-84fca46d89b7","properties": null,"rotation": 0,"scaleX": 4,"scaleY": 1,"mvc": "1.1","x": 64,"y": 40},
{"name": "inst_528780C3","id": "e87509e3-a386-44d9-8fab-7b38dba0524d","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_528780C3","objId": "82ba5719-c9fa-4a0d-a577-84fca46d89b7","properties": null,"rotation": 0,"scaleX": 5,"scaleY": 1,"mvc": "1.1","x": 72,"y": 24},
{"name": "inst_92C1BA8","id": "a3da62c6-439f-4957-b87e-ad9efc657f39","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_92C1BA8","objId": "c1acbbe3-46a7-469f-ae9b-cb79af12f86a","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 280,"y": 56},
{"name": "inst_44FE2690","id": "001d1d71-cb23-4976-a644-a83d86708d4c","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_44FE2690.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_44FE2690","objId": "3a84098b-0dac-4d6a-8cef-ffafe17b56b7","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 216,"y": 40}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "500286b7-b596-48cf-94bb-7e46d30bcb46",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4294967295 },
            "depth": 100,
            "grid_x": 16,
            "grid_y": 16,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "95433133-25d4-4df0-83bb-d0ec73e99782",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": true,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "1b4a1ab0-146d-4fef-8f19-ec4cf0f28867",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "fef72664-f0cd-4aef-b5ce-1d2043b0c968",
        "Height": 180,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 320
    },
    "mvc": "1.0",
    "views": [
{"id": "6956fc11-cd28-41b5-bc4a-6da7cf49db4f","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "af5f24f8-11ce-4bfa-ba96-08a82083c0c4","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "de872868-2bc7-485f-8f40-5f2c9eacc449","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "596a47ba-d219-4e22-a075-5b0940315227","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "5db5fa50-a17e-40d9-bd26-04cd1874c4da","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "96f747b0-42d5-4c8c-a425-cb371247447e","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "cb870403-3308-46ef-a311-99314f80b0b8","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "137e82c2-811f-4aea-bbf6-65f16e73e08a","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "d9683d6b-ce5f-43c2-a5f0-27b8fecff1ca",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": false,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}