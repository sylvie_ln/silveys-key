///@description Flip
if active {
	with all {
		if sprite_exists(sprite_index) {
			if sign(image_yscale) == 1 {
				y += abs((bbox_top-(y-sprite_yoffset))-((y+sprite_yoffset-1)-bbox_bottom));
				image_yscale = -image_yscale;	
			} else {
				image_yscale = -image_yscale;	
				y -= abs((bbox_top-(y-sprite_yoffset))-((y+sprite_yoffset-1)-bbox_bottom));
			}
		}
	}
	var bg = layer_background_get_id("Background");
	if layer_background_get_sprite(bg) == sBGGrid {
		layer_background_sprite(bg,sBGGridFlip);
	} else {
		layer_background_sprite(bg,sBGGrid);
	}
	active = false;
	audio_play_sound(sfxFlip,100,false);
	instance_destroy();
}