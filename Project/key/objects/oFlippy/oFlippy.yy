{
    "id": "56f54875-e963-4996-a6e3-3e2cae649904",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFlippy",
    "eventList": [
        {
            "id": "7a1c4f78-1e33-4865-82f7-bd93f71fb34c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "56f54875-e963-4996-a6e3-3e2cae649904"
        },
        {
            "id": "ef40da9b-16d6-4e9b-b85c-8c0362b0b42d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "56f54875-e963-4996-a6e3-3e2cae649904"
        },
        {
            "id": "50c99596-34d6-4003-a2fa-09ba54eff047",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "56f54875-e963-4996-a6e3-3e2cae649904"
        }
    ],
    "maskSpriteId": "2499c9a6-ae84-4e96-841c-6be71c10c9c5",
    "overriddenProperties": null,
    "parentObjectId": "c4374604-2bff-463b-976a-6b2c0373b839",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ecbdec58-0d0c-4eb4-a944-32a1313c96cc",
    "visible": true
}