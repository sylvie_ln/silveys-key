{
    "id": "f6a98774-4d69-4bd2-81a3-a4b760a074e6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSilveyBlockAppear",
    "eventList": [
        {
            "id": "4eacd5bf-ed49-4463-86e6-665674fa060e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "f6a98774-4d69-4bd2-81a3-a4b760a074e6"
        },
        {
            "id": "9914582a-4486-4117-84af-946dfa0b47e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f6a98774-4d69-4bd2-81a3-a4b760a074e6"
        }
    ],
    "maskSpriteId": "f5963390-0e9b-408f-97c6-434f2a81f68b",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "1bddf983-5ecf-44e4-b7f8-87dadae7d1ad",
    "visible": true
}