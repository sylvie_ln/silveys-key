{
    "id": "368cf635-f5da-45c8-857b-d41088ed214d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHeart",
    "eventList": [
        {
            "id": "90c5f6ad-8e05-4d3a-903d-2bccad28411b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "368cf635-f5da-45c8-857b-d41088ed214d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c4374604-2bff-463b-976a-6b2c0373b839",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "51bb196c-bb8d-4fe1-8bbc-957c6e344cb6",
    "visible": true
}