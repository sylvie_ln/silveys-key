{
    "id": "4e250d44-df5e-4bf9-9d9e-ddeed4940a8c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCola",
    "eventList": [
        {
            "id": "215a642a-91af-42c5-8b49-cfa6350d06c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "4e250d44-df5e-4bf9-9d9e-ddeed4940a8c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c4374604-2bff-463b-976a-6b2c0373b839",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b99fc400-6af2-400b-9f5c-562c56bcd590",
    "visible": true
}