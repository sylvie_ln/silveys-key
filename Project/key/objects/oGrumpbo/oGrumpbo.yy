{
    "id": "de05fed2-1413-42a9-a579-0edfcf5911be",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGrumpbo",
    "eventList": [
        {
            "id": "5f30c1bb-d9b1-4f49-bb8b-91b02a93474c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "de05fed2-1413-42a9-a579-0edfcf5911be"
        }
    ],
    "maskSpriteId": "2499c9a6-ae84-4e96-841c-6be71c10c9c5",
    "overriddenProperties": null,
    "parentObjectId": "c7ba7e93-1376-4b3d-92c9-f582104fbf0c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "638bb1c6-f884-4fa4-9142-fd2bd2a09609",
    "visible": true
}