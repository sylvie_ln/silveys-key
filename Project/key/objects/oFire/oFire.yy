{
    "id": "2c526aaa-d882-4b28-96e4-ec8a99716676",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFire",
    "eventList": [
        {
            "id": "0a422ee3-413d-41b7-80a4-2efee60a15d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "2c526aaa-d882-4b28-96e4-ec8a99716676"
        },
        {
            "id": "f961f78c-0ee8-43be-bd85-e0072f12da4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2c526aaa-d882-4b28-96e4-ec8a99716676"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c4374604-2bff-463b-976a-6b2c0373b839",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fe98744e-f806-4d6c-a6b7-8f41262ae6a2",
    "visible": true
}