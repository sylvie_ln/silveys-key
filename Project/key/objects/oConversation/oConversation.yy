{
    "id": "8618b6d3-1061-407a-8188-a6a7c39a2d3d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oConversation",
    "eventList": [
        {
            "id": "ffe9c4ab-a23f-42ec-bf04-178aa56e4b3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8618b6d3-1061-407a-8188-a6a7c39a2d3d"
        },
        {
            "id": "ea1644b0-24a7-42fb-aeb6-336dfae15d94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "8618b6d3-1061-407a-8188-a6a7c39a2d3d"
        },
        {
            "id": "a4ed93f6-4416-4ba9-987b-1e84cd18677d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "8618b6d3-1061-407a-8188-a6a7c39a2d3d"
        },
        {
            "id": "98cc14e1-f410-4475-a059-7941414eb95e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "8618b6d3-1061-407a-8188-a6a7c39a2d3d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}