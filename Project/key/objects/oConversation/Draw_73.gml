draw_set_color(c_black);
draw_rectangle(0,0,room_width,room_height,false);

var silvey_x = room_width div 5;
var silvey_y = (room_height div 5)*3;
var partner_x = silvey_x*4;
var partner_y = silvey_y;
draw_sprite(sSpotlightBottom,0,silvey_x,silvey_y);
with oSilvey {
	var px = x;
	var py = y;
	var ps = sprite_index;
	var pxs = image_xscale;
	x = silvey_x;
	y = silvey_y;
	sprite_index = sSilveyD;
	image_xscale = abs(image_xscale);
	draw_self();
	x = px;
	y = py;
	sprite_index = ps;
	image_xscale = pxs;
}
draw_sprite(sSpotlight,0,silvey_x,silvey_y);

draw_sprite(sSpotlightBottom,0,partner_x,partner_y);
with partner {
	var px = x;
	var py = y;
	var pxs = image_xscale;
	x = partner_x;
	y = partner_y;
	image_xscale = -abs(image_xscale);
	draw_self();
	x = px;
	y = py;
	image_xscale = pxs;
}
draw_sprite(sSpotlight,0,partner_x,partner_y);
draw_set_color(c_white);
draw_set_font(global.sylvie_font_bold);
var hpos = room_width div 2;
var vpos = silvey_y;
draw_text_centered(hpos,vpos,name);
draw_set_font(global.sylvie_font);
draw_text_centered(hpos,vpos+20,text);