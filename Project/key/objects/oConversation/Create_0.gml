depth = global.depth_above;
partner = noone;
name = "JIGGLER"
text = "Welcome to JIGGLER's Delight."


if room != rmField100 {
	audio_pause_sound(global.bgm);
	audio_play_sound(bgmConversation,101,true);
}

broadcast_message(message.conversation_start);