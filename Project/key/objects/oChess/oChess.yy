{
    "id": "4792e576-2ec8-4be0-9680-42fcff3aacab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oChess",
    "eventList": [
        {
            "id": "054bd157-b9aa-4313-b7e3-fa0717e049f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4792e576-2ec8-4be0-9680-42fcff3aacab"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c7ba7e93-1376-4b3d-92c9-f582104fbf0c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2ca87bdf-fc34-4415-a9f4-7a791f069513",
    "visible": true
}