{
    "id": "b5af6e82-736e-4738-91d1-b1fc6dba708e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSilveyBlock",
    "eventList": [
        {
            "id": "2fcb15b2-7d21-4755-a2dd-a57c74083ab8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "b5af6e82-736e-4738-91d1-b1fc6dba708e"
        },
        {
            "id": "cf04bea6-5405-4434-886c-1ded2886829a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b5af6e82-736e-4738-91d1-b1fc6dba708e"
        },
        {
            "id": "7e39abfe-0f67-43f3-8e1c-3f6e9fae4725",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "b5af6e82-736e-4738-91d1-b1fc6dba708e"
        },
        {
            "id": "01a67539-4176-4d6c-a9df-2e073a1a9cdc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "2c526aaa-d882-4b28-96e4-ec8a99716676",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b5af6e82-736e-4738-91d1-b1fc6dba708e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "82ba5719-c9fa-4a0d-a577-84fca46d89b7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "f5963390-0e9b-408f-97c6-434f2a81f68b",
    "visible": true
}