{
    "id": "11cbf5c4-0caf-423d-a5c3-27ce587e631f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLilFellow",
    "eventList": [
        {
            "id": "d32d1049-b437-4897-a7f6-4e62c7e46047",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "11cbf5c4-0caf-423d-a5c3-27ce587e631f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c7ba7e93-1376-4b3d-92c9-f582104fbf0c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0215d2db-cdbf-481a-bd2a-51b72a48cfbb",
    "visible": true
}