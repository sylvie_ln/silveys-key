if image_xscale != 1 or image_yscale != 1 {
	for(var i=0; i<image_xscale; i++) {
		for(var j=0; j<image_yscale; j++) {
			instance_create_depth((x-sprite_xoffset)+i*16+8,(y-sprite_yoffset)+j*16+8,depth,object_index);	
		}
	}
	instance_destroy();
	exit;
}