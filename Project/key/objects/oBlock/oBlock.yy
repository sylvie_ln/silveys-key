{
    "id": "82ba5719-c9fa-4a0d-a577-84fca46d89b7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBlock",
    "eventList": [
        {
            "id": "afcb6c88-6a3a-49f8-9e98-470a9129a9f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "82ba5719-c9fa-4a0d-a577-84fca46d89b7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "1b885306-4468-4779-aa2b-7ca5561da300",
    "visible": true
}