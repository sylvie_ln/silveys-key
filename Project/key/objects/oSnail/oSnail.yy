{
    "id": "2e338696-4a2e-4c98-8784-6eb871087dda",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSnail",
    "eventList": [
        {
            "id": "8c9ed413-6dc4-4805-b22d-8fdf3fe669e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2e338696-4a2e-4c98-8784-6eb871087dda"
        }
    ],
    "maskSpriteId": "2499c9a6-ae84-4e96-841c-6be71c10c9c5",
    "overriddenProperties": null,
    "parentObjectId": "c7ba7e93-1376-4b3d-92c9-f582104fbf0c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f37e991c-4039-47f7-b7e9-7511452b8c74",
    "visible": true
}