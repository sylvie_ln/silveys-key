if room_exists(target) {
	room_goto(target);
} else {
	audio_stop_all();
	game_restart();
}