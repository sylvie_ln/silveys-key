if audio_exists(global.bgm) {
	audio_stop_sound(global.bgm);
}
active = true;
var rm = room_get_name(room);
if string_copy(rm,3,5) = "Field" {
	target = -1;
	global.field = real(string_extract(rm,8,string_length(rm)));
	if global.field == 0 {
		global.field = global.selected_field;	
	}
	global.visited[global.field] = true;
	save_globals();
	state = sHUD.pre_level;
	audio_play_sound(sfxAction,100,false);
	global.bgm = bgmRoom;
	color = make_color_hsl_240(190,239,90);
	if instance_exists(oBlockFire) {
		global.bgm = bgmRoomFire;	
		color = make_color_hsl_240(230,239,90);
	}
	if instance_exists(oBlockEnd) {
		global.bgm = bgmRoomEnd;	
		color = make_color_hsl_240(210,239,90);
	}
	alarm[0] = room_speed*2.5;
} else {
	active = false;	
}