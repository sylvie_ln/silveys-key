while !ds_queue_empty(messages) {
	var m = ds_queue_dequeue(messages);
	var m_type = m[0];
	switch(m_type) {
		case message.level_end:
			state = sHUD.post_level;
			audio_stop_sound(global.bgm);
			audio_play_sound(sfxUnaction,100,false);
			target = m[1];
			alarm[1] = room_speed*2.5;
		break;
		case message.level_start:
			state = sHUD.level;
		break;
	}
}

/*
if room != rmEnd {
	if state == sHUD.pre_level or state == sHUD.post_level {
		if input_pressed("Block") {
			if state == sHUD.pre_level {
				alarm[0] = 1;	
			} else {
				alarm[1] = 1;
			}
		}
	}
}
*/

if keyboard_check_pressed(vk_escape) {
	audio_stop_all();
	game_restart();
	exit;
}

if active and state == sHUD.level {
	if input_pressed("Reset") {
		room_restart();
		exit;
	}
	global.life -= 10;
}
