global.life = 100000000000;
global.field = 0;
depth = global.depth_middle;
active = false;
target = -1;
messages = ds_queue_create();

state = sHUD.pre_level;

enum sHUD {
	pre_level,
	level,
	post_level
}
color = c_black;