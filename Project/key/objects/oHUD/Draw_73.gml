if !active { exit; }
if state == sHUD.level or state == sHUD.post_level{
	draw_set_color(color);
	draw_rectangle(x,y,room_width,room_height,false);
	draw_set_color(c_white);
	draw_set_font(global.sylvie_font_bold_2x);
	draw_set_halign(fa_left);
	if state == sHUD.post_level {
		draw_text_centered(room_width div 2,y-1,"Silvey Completed A Field!");
	} else {
		draw_text(x+4,y-1,"FIELD "+string_replace_all(string_formatt(global.field,3,0)," ","0"));
		draw_set_halign(fa_right);
		draw_text(room_width-1,y-1,"LIFE "+number_format(global.life));
		draw_set_halign(fa_left);
	}
} else if state == sHUD.pre_level {
	draw_set_color(color);
	draw_rectangle(0,0,room_width,room_height,false);
	draw_set_color(c_white);
	draw_set_font(global.sylvie_font_bold_2x);
	draw_text_centered(room_width div 2, room_height div 3,
	"FIELD "+string_replace_all(string_formatt(global.field,3,0)," ","0")+"\n"
	+"Action!");
	draw_set_font(global.sylvie_font);
	draw_text_centered(room_width div 2, room_height-48,get_name(global.field));
}