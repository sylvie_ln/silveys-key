{
    "id": "8c6808a4-dea8-441b-a8c1-1cdc61642bc8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHUD",
    "eventList": [
        {
            "id": "5cddb6c7-c966-4276-816e-99042b4222b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "8c6808a4-dea8-441b-a8c1-1cdc61642bc8"
        },
        {
            "id": "1f1a2644-c21c-409c-b9aa-861513dcb31b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8c6808a4-dea8-441b-a8c1-1cdc61642bc8"
        },
        {
            "id": "1a1e2bb2-cbd4-4af4-b769-c446b75ecb12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8c6808a4-dea8-441b-a8c1-1cdc61642bc8"
        },
        {
            "id": "ff72ef3c-23a4-4e1a-a9bb-290b749d7d4a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8c6808a4-dea8-441b-a8c1-1cdc61642bc8"
        },
        {
            "id": "12e0c79c-42dd-4228-9db4-cfebf62ca7b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "8c6808a4-dea8-441b-a8c1-1cdc61642bc8"
        },
        {
            "id": "b26f0c55-2504-4abe-b874-15ddf394a4b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "8c6808a4-dea8-441b-a8c1-1cdc61642bc8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}