if input_pressed("Block") {
	var rm = asset_get_index("rmField"+string(global.selected_field));
	if room_exists(rm) {
		room_goto(rm);
	} else {
		room_goto(rmField0);	
	}
}	

if keyboard_check_pressed(vk_escape) {
	game_end();
	exit;
}

if input_pressed_repeat("Up") {
	global.selected_field += 10;
	global.selected_field = global.selected_field mod 100;
}

if input_pressed_repeat("Down") {
	global.selected_field -= 10;
	if global.selected_field < 0 {
		global.selected_field += 100;
	}
}

if input_pressed_repeat("Right") {
	global.selected_field += 1;
	global.selected_field = global.selected_field mod 100;
}

if input_pressed_repeat("Left") {
	global.selected_field -= 1;
	if global.selected_field < 0 {
		global.selected_field += 100;
	}
}