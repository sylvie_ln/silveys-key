{
    "id": "bd33620c-577c-44ab-bbe1-f267273a4005",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTitle",
    "eventList": [
        {
            "id": "c25112e6-f1fa-40ca-a8dc-585e95edacb7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "bd33620c-577c-44ab-bbe1-f267273a4005"
        },
        {
            "id": "5f0102c7-76fb-47c2-aaa5-a81f3cda30b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bd33620c-577c-44ab-bbe1-f267273a4005"
        },
        {
            "id": "41ecf78f-e31e-4e3f-b8d5-fc172537b82b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bd33620c-577c-44ab-bbe1-f267273a4005"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}