draw_set_font(global.sylvie_font_bold_2x);
draw_set_color(c_white);
draw_text_centered(room_width div 2, room_height div 4, "Silvey's Key\nPress Control or C to Start");
draw_set_font(global.sylvie_font_bold);
draw_text_centered(room_width div 2, (room_height div 2)+24,"Field Select\n< "+
string_replace_all(string_formatt(global.selected_field,2,0)," ","0")
+" >");
draw_set_font(global.sylvie_font);
draw_text_centered(room_width div 2, 2,"Arrows/WASD to Move ~ Up/X/Space to Jump ~ Down to Sit\nControl/C to Magic ~ R to Reset ~ Escape to Quit");
if global.visited[global.selected_field] {
	var t = get_name(global.selected_field);
	draw_text_centered(room_width div 2, room_height-24, t);
}