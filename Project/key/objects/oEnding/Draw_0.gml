draw_set_font(global.sylvie_font_bold_2x);
draw_set_color(c_white);
var t = "The End!";
switch(global.field) {
	case 10:
		t = "Normal Ending\n\"Is that all...?\""
	break;
	case 24:
		t = "Secret Ending\n\"Secret path!\""
	break;
	case 45:
		t = "Secret Ending\n\"Phew, close one!\""
	break;
	case 55:
	case 56:
	case 57:
	case 58:
	case 59:
	case 60:
	case 61:
	case 62:
		t = "Secret Ending\n\"Mysterious depths...\""
	break;
	case 95:
		t = "Secret Ending\n\"That was fast!\""
	break;
	case 100:
		t = "True Ending\n\"Kittey Paradise!!\""
	break;
	default:
		t = "Normal Ending\n\"A Friendley Visit\"";
	break;
}
draw_text_centered(room_width div 2, room_height div 2-24, t);