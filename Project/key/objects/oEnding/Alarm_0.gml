switch(global.field) {
	case 10:
		audio_play_sound(sfxAction,101,false);
	break;
	case 24:
		audio_play_sound(bgmRoom,101,true);
	break;
	case 55:
	case 56:
	case 57:
	case 58:
	case 59:
	case 60:
	case 61:
	case 62:
		audio_play_sound(bgmConversation,101,true);
	break;
	case 45:
	case 95:
		audio_play_sound(bgmRoomFire,101,true);
	break;
	case 100:
		audio_play_sound(bgmRoomEnd,101,true);
	break;
	default:
		audio_play_sound(sfxAction,101,false);
	break;
}