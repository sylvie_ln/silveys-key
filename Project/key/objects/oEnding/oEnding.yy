{
    "id": "5bbb716b-0137-4315-97d4-ae035baccbaf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnding",
    "eventList": [
        {
            "id": "675880ee-50ac-4403-88bf-c202afe63a13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5bbb716b-0137-4315-97d4-ae035baccbaf"
        },
        {
            "id": "4dca4630-d4d4-42c8-a706-c06084dda104",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5bbb716b-0137-4315-97d4-ae035baccbaf"
        },
        {
            "id": "872e968f-d32d-462f-99f1-3ced2c049ee3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5bbb716b-0137-4315-97d4-ae035baccbaf"
        },
        {
            "id": "d746fa0e-89c5-4c00-a16c-a3c2ca06dadf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5bbb716b-0137-4315-97d4-ae035baccbaf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}