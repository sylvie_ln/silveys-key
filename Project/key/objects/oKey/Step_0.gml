if flying and !movement_paused {
	var door = instance_nearest(x,y,oDoor);
	if door == noone {
		door = oSilvey;
	}
	if oSilvey.sleep {
		door = oSilvey;
	}
	var dir = point_direction(x,y,door.x,door.y);
	hv += lengthdir_x(acc,dir);
	vv += lengthdir_y(acc,dir);
	var dist = point_distance(0,0,hv,vv); 
	if dist > spd {
		hv = hv/dist*spd;
		vv = vv/dist*spd;
	}
	mask_index = sprite_index;
	
	with oGrumpbo {
		solid = true;	
	}
	
	if !move(hv,0) {
		hv = -hv*1.1;
	}
	if !move(vv,1) {
		vv = -vv*1.1;	
	}
	
	with oGrumpbo {
		solid = false;	
	}
	
	mask_index = sJumpOverMask;
}