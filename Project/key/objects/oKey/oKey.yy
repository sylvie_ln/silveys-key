{
    "id": "1efc200f-1458-4af7-ad13-39cc75d86d33",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oKey",
    "eventList": [
        {
            "id": "94c0495b-dc2e-44e7-a4a9-c1bd837c113d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1efc200f-1458-4af7-ad13-39cc75d86d33"
        },
        {
            "id": "18a8ac02-959e-41b1-95a4-138bb02e6eae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "1efc200f-1458-4af7-ad13-39cc75d86d33"
        },
        {
            "id": "ead923c7-222d-4b53-9822-2a8d522451e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1efc200f-1458-4af7-ad13-39cc75d86d33"
        },
        {
            "id": "a9c64c6d-015e-441f-b87b-93c323b9e9ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ade19e8b-e1e1-43b6-baa8-5bfd1a76960a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1efc200f-1458-4af7-ad13-39cc75d86d33"
        },
        {
            "id": "6ea15225-f149-46d2-9f08-ef77fb68d3d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "2c526aaa-d882-4b28-96e4-ec8a99716676",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1efc200f-1458-4af7-ad13-39cc75d86d33"
        },
        {
            "id": "52a28603-e6c1-4c06-a1f7-296b5fa173ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a6fe2338-a38a-4a15-ab93-bcc5da718686",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1efc200f-1458-4af7-ad13-39cc75d86d33"
        }
    ],
    "maskSpriteId": "2499c9a6-ae84-4e96-841c-6be71c10c9c5",
    "overriddenProperties": null,
    "parentObjectId": "9529a30b-2e50-49c1-945e-16bc58b3c9ae",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ebafa70e-e6dc-463c-b3df-302e9a954c27",
    "visible": true
}