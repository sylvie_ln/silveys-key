if active {
	if room == rmField6 {
		if diggler == global.diggler+1 {
			global.diggler = diggler;
		} else if global.diggler != -1 and global.diggler != 5 {
			global.diggler = 0;	
			if diggler == 1 {
				global.diggler = 1;
			}
		}
		switch(global.diggler) {
			case -1:
				text = 
				@"Talk to us five in order from weakest to strongest.
				You will learn our secret."
				global.diggler = 0;
			break;
			case 5:
				text = 
				@"Okay, the secret of the Digglers is....
				That there is no secret of the Digglers at all.
				We just wanted to make a fun puzzle. I'm sorry."
			break;
			default:
				text = diggler_text;
			break;
		}
	}
}
event_inherited();