{
    "id": "ccda5772-56c9-4a8a-a101-921b5ea52a38",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDiggler",
    "eventList": [
        {
            "id": "5e9abe1b-bfae-48f0-b3cf-835eccd88c60",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ccda5772-56c9-4a8a-a101-921b5ea52a38"
        },
        {
            "id": "42d6528f-494b-4625-99cc-6ea8cb02b435",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "ccda5772-56c9-4a8a-a101-921b5ea52a38"
        }
    ],
    "maskSpriteId": "2499c9a6-ae84-4e96-841c-6be71c10c9c5",
    "overriddenProperties": null,
    "parentObjectId": "c7ba7e93-1376-4b3d-92c9-f582104fbf0c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "91f9a7c3-42c1-430a-8c4d-0dbeddc5b0f3",
    "visible": true
}