{
    "id": "10a0eac7-f40a-4e1d-aa27-3b459361adea",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHint",
    "eventList": [
        {
            "id": "f5dd28f2-1448-4adb-b972-578dc5f86abf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 20,
            "eventtype": 7,
            "m_owner": "10a0eac7-f40a-4e1d-aa27-3b459361adea"
        },
        {
            "id": "f617bb3d-95d1-4c0b-98f6-332d52222c06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "10a0eac7-f40a-4e1d-aa27-3b459361adea"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c4374604-2bff-463b-976a-6b2c0373b839",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
    "visible": true
}