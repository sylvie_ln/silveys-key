{
    "id": "bf4ba4d3-726e-431c-b99c-bd247745eb7f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHiddenBlockFire",
    "eventList": [
        {
            "id": "e6918bd8-883b-47bb-929c-37bbae1086f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 20,
            "eventtype": 7,
            "m_owner": "bf4ba4d3-726e-431c-b99c-bd247745eb7f"
        },
        {
            "id": "783089e3-98d1-43b6-ae54-1005759bb6cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bf4ba4d3-726e-431c-b99c-bd247745eb7f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c4374604-2bff-463b-976a-6b2c0373b839",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "e8b2002e-55ad-44c2-b4c3-88b610dbed7b",
    "visible": true
}