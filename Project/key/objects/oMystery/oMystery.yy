{
    "id": "d86d5fca-87ad-4056-be9d-3a82e24faf0e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMystery",
    "eventList": [
        {
            "id": "3acd5ab7-84fb-48b7-b521-89d9be6d7332",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "d86d5fca-87ad-4056-be9d-3a82e24faf0e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c4374604-2bff-463b-976a-6b2c0373b839",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e8c69112-ed85-4d80-8cff-1ac6f54aee1b",
    "visible": true
}