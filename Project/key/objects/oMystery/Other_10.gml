with oBlock {
	if object_index == oSilveyBlock {
		instance_change(oBlock,false);	
	} else {
		if x > 8 and x < room_width-8 and y > 8 and y < room_height - 8 - 20 {
			instance_change(oSilveyBlock,false);
			event_perform_object(oSilveyBlock,ev_create,0);
		}
	}
}
audio_play_sound(sfxMystery,100,false);
instance_destroy();