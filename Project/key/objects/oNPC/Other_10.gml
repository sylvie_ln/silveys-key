if (ong or !grav_on) and active {
	var c = instance_create_depth(0,0,depth,oConversation);
	c.name = name;
	c.text = text;
	c.partner = id;
	audio_play_sound(talksound,100,0);
	active = false;	
	if quest_complete == 1 {
		if quest_flag != -1 {
			with all {
				if id == other.id { continue; }
				if variable_instance_exists(id,"quest_flag") {
					if quest_flag == other.quest_flag {
						instance_destroy();	
					}
				}
			}	
		}	
		quest_complete = 2;
		text = post_quest;
	}
}