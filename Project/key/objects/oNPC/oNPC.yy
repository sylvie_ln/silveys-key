{
    "id": "c7ba7e93-1376-4b3d-92c9-f582104fbf0c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oNPC",
    "eventList": [
        {
            "id": "07207208-01ba-4f81-9755-19b75083d935",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c7ba7e93-1376-4b3d-92c9-f582104fbf0c"
        },
        {
            "id": "dcd0ccda-d454-46a9-8c63-59b74636dc45",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "c7ba7e93-1376-4b3d-92c9-f582104fbf0c"
        },
        {
            "id": "18b1df3f-ec94-433f-879c-6f9e127dea75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "c7ba7e93-1376-4b3d-92c9-f582104fbf0c"
        },
        {
            "id": "38e2905f-0d75-4833-a74b-03f902d1422e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "c7ba7e93-1376-4b3d-92c9-f582104fbf0c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c4374604-2bff-463b-976a-6b2c0373b839",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}