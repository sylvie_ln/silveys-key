{
    "id": "6d8c85b1-848d-4c04-986e-1250feb04380",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBlockEnd",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "82ba5719-c9fa-4a0d-a577-84fca46d89b7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "a2e2790b-2329-4a3f-86ff-0f68d6507245",
    "visible": true
}