if !hidden {
	if item != noone {
		var inst = instance_create_depth(x,y,depth,item);
		with inst {
			event_user(11);
		}
		inst.movement_paused = false;
		inst.action_paused = false;
		if inst.object_index == oDoor {
			inst.target[0] = target;	
		}
		if object_is_ancestor(inst.object_index,oNPC) {
			inst.text = text;
		}
		instance_destroy();
		exit;
	} else {
		image_index++;	
	}
} else {
	hidden = false;	
}
event_inherited();