{
    "id": "3a0193af-b639-485b-80e1-952140b448ba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFlower",
    "eventList": [
        {
            "id": "48d9b6dc-023b-49ea-9ef9-57a2aca8c0ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3a0193af-b639-485b-80e1-952140b448ba"
        },
        {
            "id": "9a117362-3019-495f-bded-57247193647a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 20,
            "eventtype": 7,
            "m_owner": "3a0193af-b639-485b-80e1-952140b448ba"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c4374604-2bff-463b-976a-6b2c0373b839",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ba8f15e9-ce18-4bcd-9d39-a74afec01ac3",
    "visible": true
}