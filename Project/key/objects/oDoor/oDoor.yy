{
    "id": "ade19e8b-e1e1-43b6-baa8-5bfd1a76960a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDoor",
    "eventList": [
        {
            "id": "ad5ac927-851c-42ce-98e2-fc589693a702",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ade19e8b-e1e1-43b6-baa8-5bfd1a76960a"
        },
        {
            "id": "4bb36dae-6c22-4ee7-a9c9-ac3872e31412",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ade19e8b-e1e1-43b6-baa8-5bfd1a76960a"
        },
        {
            "id": "706a74da-a489-4c9d-9723-4b097e85f6c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "ade19e8b-e1e1-43b6-baa8-5bfd1a76960a"
        },
        {
            "id": "ce83852c-e3ba-46ad-b438-92febd1f8838",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "ade19e8b-e1e1-43b6-baa8-5bfd1a76960a"
        },
        {
            "id": "fcee226b-a5c4-43c4-9166-1d22821142c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 21,
            "eventtype": 7,
            "m_owner": "ade19e8b-e1e1-43b6-baa8-5bfd1a76960a"
        }
    ],
    "maskSpriteId": "2499c9a6-ae84-4e96-841c-6be71c10c9c5",
    "overriddenProperties": null,
    "parentObjectId": "9529a30b-2e50-49c1-945e-16bc58b3c9ae",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7586a1ee-46ef-4158-a9ee-337ef96d1674",
    "visible": true
}