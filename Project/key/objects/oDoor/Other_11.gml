//@description Open/Close
open = !open;
if open {
	audio_play_sound(sfxUnlock,100,false);	
} else {
	audio_play_sound(sfxLock,100,false);
}