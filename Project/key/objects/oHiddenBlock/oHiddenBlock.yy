{
    "id": "3d04fb60-a857-48e4-928a-35c1e5c8ef61",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHiddenBlock",
    "eventList": [
        {
            "id": "ee1f7f1c-e795-4166-9dfa-ca23b2a55f9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 20,
            "eventtype": 7,
            "m_owner": "3d04fb60-a857-48e4-928a-35c1e5c8ef61"
        },
        {
            "id": "6c1684a6-f637-4885-b126-8aa10e02ef56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3d04fb60-a857-48e4-928a-35c1e5c8ef61"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c4374604-2bff-463b-976a-6b2c0373b839",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "add96877-8835-4728-9dae-8d4754332389",
    "visible": true
}