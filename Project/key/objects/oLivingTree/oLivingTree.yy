{
    "id": "f5246ac2-de93-48d4-8e2e-f34c64d49e90",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLivingTree",
    "eventList": [
        {
            "id": "b15801a7-9c65-407f-9797-20efa302b685",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f5246ac2-de93-48d4-8e2e-f34c64d49e90"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c7ba7e93-1376-4b3d-92c9-f582104fbf0c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "734bd1b5-7d2e-4499-8b20-e763b471740d",
    "visible": true
}