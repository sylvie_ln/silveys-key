{
    "id": "333d8b97-04d4-4565-8165-e1cbdc7387aa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFrog2",
    "eventList": [
        {
            "id": "6085cd9a-99a7-45fc-9b0d-d36cc828bc7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "333d8b97-04d4-4565-8165-e1cbdc7387aa"
        }
    ],
    "maskSpriteId": "2499c9a6-ae84-4e96-841c-6be71c10c9c5",
    "overriddenProperties": null,
    "parentObjectId": "c7ba7e93-1376-4b3d-92c9-f582104fbf0c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c85e6e8d-5291-4970-82c4-1da1d34eb3e7",
    "visible": true
}