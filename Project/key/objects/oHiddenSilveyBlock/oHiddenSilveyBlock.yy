{
    "id": "c1acbbe3-46a7-469f-ae9b-cb79af12f86a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHiddenSilveyBlock",
    "eventList": [
        {
            "id": "16885567-5176-4fa7-9b13-aca63982a7ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c1acbbe3-46a7-469f-ae9b-cb79af12f86a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b5af6e82-736e-4738-91d1-b1fc6dba708e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "add96877-8835-4728-9dae-8d4754332389",
    "visible": true
}