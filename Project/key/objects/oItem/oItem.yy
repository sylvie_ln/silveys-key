{
    "id": "c4374604-2bff-463b-976a-6b2c0373b839",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oItem",
    "eventList": [
        {
            "id": "0f90cf43-555b-4c6a-ba35-032cc6736d80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c4374604-2bff-463b-976a-6b2c0373b839"
        },
        {
            "id": "7222a19c-a75a-4aa6-b9ec-139ac0325cc1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 20,
            "eventtype": 7,
            "m_owner": "c4374604-2bff-463b-976a-6b2c0373b839"
        },
        {
            "id": "253e72e6-418b-40b3-bc96-8dfbb7df0519",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "c4374604-2bff-463b-976a-6b2c0373b839"
        },
        {
            "id": "4db254d6-7c82-44db-a915-ec91b38128e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "fad5ad96-3049-4ab8-a204-b6b2d9305329",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c4374604-2bff-463b-976a-6b2c0373b839"
        },
        {
            "id": "f64d6ac1-5750-4e05-bf55-a6bb919f3d6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "c4374604-2bff-463b-976a-6b2c0373b839"
        },
        {
            "id": "2bce0d3e-ebd1-4b71-83ff-0f7683a1ddba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "c4374604-2bff-463b-976a-6b2c0373b839"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}