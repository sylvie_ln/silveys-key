while !ds_queue_empty(deferred_messages) {
	ds_queue_enqueue(messages,ds_queue_dequeue(deferred_messages));	
}
while !ds_queue_empty(messages) {
	var m = ds_queue_dequeue(messages);
	var m_type = m[0];
	switch(m_type) {
		case message.level_end:
		case message.conversation_start:
			movement_paused = true;
			action_paused = true;
		break;
		case message.level_start:
		case message.conversation_end:
			movement_paused = false;
			action_paused = false;
		break;
	}
}

if movement_paused { exit; }
if !hidden and grav_on {
	var prev_mask = mask_index;
	mask_index = sprite_index;

	ong = !place_free(x,y+1);

	if !ong {
		vv += grav;	
	}

	if !move(vv,1) { vv = 0; }

	mask_index = prev_mask;	
}