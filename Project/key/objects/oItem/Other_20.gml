///@description Hide/Unhide
if hidden { 
	visible = false; 
} else {
	visible = true;	
}