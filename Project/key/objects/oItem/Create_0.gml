hidden = false;
grav_on = false;

ong = !place_free(x,y+1);
grav = 0.2;
vv = 0;

depth = global.depth_below;

remainder = [0,0];
check_collision = check_collision_list;
handle_collision = handle_collision_list;

if object_get_mask(object_index) == -1 {
	mask_index = sJumpOverMask;
}

movement_paused = true;
action_paused = true;

deferred_messages = ds_queue_create();
messages = ds_queue_create();