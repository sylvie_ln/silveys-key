{
    "id": "fad5ad96-3049-4ab8-a204-b6b2d9305329",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSilveyBlockDisappear",
    "eventList": [
        {
            "id": "abe437ec-7194-4d61-9e52-5af5302f3ddb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "fad5ad96-3049-4ab8-a204-b6b2d9305329"
        },
        {
            "id": "339d37a8-624e-4fab-bfb3-fc790932df7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fad5ad96-3049-4ab8-a204-b6b2d9305329"
        }
    ],
    "maskSpriteId": "f5963390-0e9b-408f-97c6-434f2a81f68b",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "9bc09227-408b-44d3-80d2-25c51c260c5c",
    "visible": true
}