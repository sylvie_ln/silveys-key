if !action_paused {
	if input_pressed("Block") {
		block = true;	
		movement_paused = true;
		image_index = 0;
		if image_xscale == 1 {
			var xp = ceil((bbox_right-block_dist+1)/16)*16 + 8; 	
		} else {
			var xp = floor((bbox_left+block_dist)/16)*16 - 8;
		}
		if ducking {
			var yp = floor((y+16)/16)*16 + 8;
		} else {
			var yp = floor(y/16)*16 + 8;
		}
	
		mask_index = sSilveyBlock;
	
		var b = instance_place(xp,yp,oSilveyBlock);
		if b == noone {
			var b = instance_place(xp,yp,oSilveyBlockAppear);
			if b == noone {
				var b = instance_place(xp,yp,oSilveyBlockDisappear);
				if b == noone {
					if place_free(xp,yp) {
						b = instance_create_depth(xp,yp,depth,oSilveyBlockAppear);
						b.image_yscale = image_yscale;
						mask_index = sSilveyMask;
						repeat(block_dist) {
							if place_free(x,y) { break; }
							x -= image_xscale;	
						}
					}
				} else {
					with b {
						instance_change(oSilveyBlockAppear,false);	
						image_index = image_number - image_index;
					}
				}
			} else {
				with b {
					instance_change(oSilveyBlockDisappear,false);	
					image_index = image_number - image_index;
				}
			}
		} else {
			with b {
				instance_destroy();	
			}
		}
	
		mask_index = sSilveyMask;
	}
}

var hdir = 0;
var ong = !place_free(x,y+1);
if !ong { ducking = false; }

if !movement_paused {
	var left = input_held("Left");
	var right = input_held("Right");

	hdir = right-left;
	if left and right {
		hdir = (input_held_time(left) <= input_held_time(right)) ? -1 : 1;	
	}

	hv = hdir*spd;
	
	if ong {
		ducking = input_held("Down"); 
	}

	if !ong {
		vv += grav;	
	}

	if ong and input_pressed("Jump") {
		vv = -jmp;
		/*
		audio_sound_pitch(sfxJump,random_range(0.9,1.1));
		audio_play_sound(sfxJump,100,false);
		*/
		ducking = false;
	}

	if !ducking {
		if !move(hv,0) { hv = 0; }
		if !move(vv,1) { vv = 0; }
	}
}

if ducking {
	sleep_timer++;
	if sleep_timer >= sleep_time {
		sleep = true;	
	}
} else {
	sleep = false;
	sleep_timer = 0;	
}

if burn {
	image_blend = merge_color(c_red,c_white,0.5);	
	spd = default_spd*2;	
	image_speed = 2;
}

if hdir != 0 {
	image_xscale = hdir;	
}

var sstr = "sSilvey";
if block {
	if ducking {
		sstr += "D";	
	}
	sstr += "B";
} else {
	if ong {
		if ducking {
			sstr += "D";	
		} else if hv != 0 {
			sstr += "W";
		} else {
			sstr += "S";
		}
	} else {
		if vv < 0 {
			sstr += "J"	
		} else {
			sstr += "F";
		}
	}
}

with instance_place(x,y,oSilveyBlock) {
	instance_destroy();	
}

sprite_index = asset_get_index(sstr);