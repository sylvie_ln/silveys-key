{
    "id": "a88c679d-d029-40d9-bbf6-d22448fe97d4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSilvey",
    "eventList": [
        {
            "id": "babe0e93-eaff-4802-b517-409c8ac8a171",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a88c679d-d029-40d9-bbf6-d22448fe97d4"
        },
        {
            "id": "f2416639-0238-4335-8492-a0bad95b66ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a88c679d-d029-40d9-bbf6-d22448fe97d4"
        },
        {
            "id": "396c05a2-c8cc-4d19-a294-6a0e320d0d15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "a88c679d-d029-40d9-bbf6-d22448fe97d4"
        },
        {
            "id": "7b0e96ce-436e-42e7-b761-63f7ac53f110",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "a88c679d-d029-40d9-bbf6-d22448fe97d4"
        }
    ],
    "maskSpriteId": "6f3ffb08-5b8a-4fb0-b3db-bc9ca318521a",
    "overriddenProperties": null,
    "parentObjectId": "9529a30b-2e50-49c1-945e-16bc58b3c9ae",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fb4e45b0-572b-4595-b103-69ad556179cc",
    "visible": true
}