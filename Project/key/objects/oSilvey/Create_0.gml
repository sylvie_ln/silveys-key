event_inherited();

default_spd = 1.5;
spd = default_spd;
grav = 0.2;
jmp = sqrt(2*grav*16.5);

hv = 0;
vv = 0;

ducking = false;
sleep_timer = 0;
sleep_time = 5*room_speed;
sleep = false;
ii = 0;

burn = false;

block = false;
block_dist = 4;

handle_collision = handle_collision_player;

depth = global.depth_middle;

enum message {
	level_start,
	level_end,
	conversation_start,
	conversation_end	
}