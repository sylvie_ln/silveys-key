{
    "id": "1f0b65ab-2219-4c9d-9e0d-eec670103a0e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oChuckster",
    "eventList": [
        {
            "id": "0231914f-c410-4643-b085-ff006309a785",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "1f0b65ab-2219-4c9d-9e0d-eec670103a0e"
        },
        {
            "id": "091e8bd2-fb1c-4ad1-aa1e-1623f5882a72",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1efc200f-1458-4af7-ad13-39cc75d86d33",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1f0b65ab-2219-4c9d-9e0d-eec670103a0e"
        },
        {
            "id": "b438232e-0f56-4863-aea0-c95fd30508ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1f0b65ab-2219-4c9d-9e0d-eec670103a0e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c4374604-2bff-463b-976a-6b2c0373b839",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "29921b18-5854-4b3c-9213-7323edb00b4b",
    "visible": true
}