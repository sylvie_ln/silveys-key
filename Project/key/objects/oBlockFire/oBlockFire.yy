{
    "id": "bf6bb1ba-440c-4cd6-896f-488204332c85",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBlockFire",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "82ba5719-c9fa-4a0d-a577-84fca46d89b7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "24d01db3-a0d0-4f9c-af73-526e3341db6f",
    "visible": true
}