{
    "id": "a6fe2338-a38a-4a15-ab93-bcc5da718686",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWarp",
    "eventList": [
        {
            "id": "9f607bac-9edf-4466-ba91-a4ec4011bdf5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "a6fe2338-a38a-4a15-ab93-bcc5da718686"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c4374604-2bff-463b-976a-6b2c0373b839",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e63f24c0-62da-489a-87f8-801f419dc8e7",
    "visible": true
}