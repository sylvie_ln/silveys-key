{
    "id": "50bb9b91-3214-4a6c-b688-17244096a935",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oColumn",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "82ba5719-c9fa-4a0d-a577-84fca46d89b7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "eeac860d-1154-4dc6-bab2-9cbefbec08d3",
    "visible": true
}