{
    "id": "3a84098b-0dac-4d6a-8cef-ffafe17b56b7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFrog",
    "eventList": [
        {
            "id": "b9a61ecb-eb71-4ca8-8055-040da7c16117",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3a84098b-0dac-4d6a-8cef-ffafe17b56b7"
        }
    ],
    "maskSpriteId": "2499c9a6-ae84-4e96-841c-6be71c10c9c5",
    "overriddenProperties": null,
    "parentObjectId": "c7ba7e93-1376-4b3d-92c9-f582104fbf0c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ac64d2d6-800b-47fe-9daa-6aba0664a16e",
    "visible": true
}