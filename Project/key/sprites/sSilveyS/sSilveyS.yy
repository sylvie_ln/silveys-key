{
    "id": "fb4e45b0-572b-4595-b103-69ad556179cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSilveyS",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d9e1467-ad86-4564-8dcb-eb44b1812239",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb4e45b0-572b-4595-b103-69ad556179cc",
            "compositeImage": {
                "id": "adae54e1-0461-41f6-b270-12b40694a164",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d9e1467-ad86-4564-8dcb-eb44b1812239",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa1961e1-30cb-4e6e-aeeb-11ab249cb3e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d9e1467-ad86-4564-8dcb-eb44b1812239",
                    "LayerId": "626f68dc-cdb8-4c5b-8409-cc6d781517fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "626f68dc-cdb8-4c5b-8409-cc6d781517fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb4e45b0-572b-4595-b103-69ad556179cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}