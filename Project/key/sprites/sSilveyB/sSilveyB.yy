{
    "id": "c8c2c6ba-fd8c-4d14-9bab-0d6a7a832337",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSilveyB",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c618a199-2958-4ac7-b0d1-9325ea07716a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8c2c6ba-fd8c-4d14-9bab-0d6a7a832337",
            "compositeImage": {
                "id": "54cd748f-0e47-4d54-a815-4089438b55c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c618a199-2958-4ac7-b0d1-9325ea07716a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39526461-e112-46f2-adec-c59d14ac43fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c618a199-2958-4ac7-b0d1-9325ea07716a",
                    "LayerId": "3480a7f9-0658-4c72-a9de-fc9de48793b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3480a7f9-0658-4c72-a9de-fc9de48793b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c8c2c6ba-fd8c-4d14-9bab-0d6a7a832337",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}