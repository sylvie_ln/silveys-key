{
    "id": "7586a1ee-46ef-4158-a9ee-337ef96d1674",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDoor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "75f13a3d-fa2b-42d7-9497-d26a91073622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7586a1ee-46ef-4158-a9ee-337ef96d1674",
            "compositeImage": {
                "id": "d691579c-b56b-4e11-9885-04c6e36519fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75f13a3d-fa2b-42d7-9497-d26a91073622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc3d6002-53ff-488f-830c-fab9261a3173",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75f13a3d-fa2b-42d7-9497-d26a91073622",
                    "LayerId": "9ebf309b-b0c4-4993-97a9-e587c91b0072"
                }
            ]
        },
        {
            "id": "25a3a296-c929-4afb-8135-43776c19df3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7586a1ee-46ef-4158-a9ee-337ef96d1674",
            "compositeImage": {
                "id": "46b1abf1-2c5f-4607-8933-b4ac7da2476c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25a3a296-c929-4afb-8135-43776c19df3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af86d977-0841-4f58-8979-b2a6315a1c7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25a3a296-c929-4afb-8135-43776c19df3d",
                    "LayerId": "9ebf309b-b0c4-4993-97a9-e587c91b0072"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9ebf309b-b0c4-4993-97a9-e587c91b0072",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7586a1ee-46ef-4158-a9ee-337ef96d1674",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}