{
    "id": "eeac860d-1154-4dc6-bab2-9cbefbec08d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sColumn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 6,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38232d9c-b3ce-4577-b084-19608e21e2ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eeac860d-1154-4dc6-bab2-9cbefbec08d3",
            "compositeImage": {
                "id": "46581a64-6e27-4c69-95b4-97173500029e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38232d9c-b3ce-4577-b084-19608e21e2ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2b59caf-0a8b-4522-b92d-e1076e8079fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38232d9c-b3ce-4577-b084-19608e21e2ed",
                    "LayerId": "4905d7ff-e2a5-4bb0-a96f-6ff146421251"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4905d7ff-e2a5-4bb0-a96f-6ff146421251",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eeac860d-1154-4dc6-bab2-9cbefbec08d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}