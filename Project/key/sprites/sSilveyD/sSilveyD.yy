{
    "id": "e2615f35-eb3c-4e24-877e-0b27ebf8664c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSilveyD",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a64c12b6-ce47-4942-9eb4-1dde893bbfeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2615f35-eb3c-4e24-877e-0b27ebf8664c",
            "compositeImage": {
                "id": "e685650e-50f2-4e62-8887-641ea09e02d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a64c12b6-ce47-4942-9eb4-1dde893bbfeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6440baeb-14ad-4731-adc0-50741da71959",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a64c12b6-ce47-4942-9eb4-1dde893bbfeb",
                    "LayerId": "024d7a2e-29b5-43bf-a80f-135c576194d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "024d7a2e-29b5-43bf-a80f-135c576194d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e2615f35-eb3c-4e24-877e-0b27ebf8664c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}