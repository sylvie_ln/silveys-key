{
    "id": "89754024-d5c6-4252-b2df-cbb152bd5ffa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBabis",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99cdc73e-5176-435a-84b6-7aa6d67ed5b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89754024-d5c6-4252-b2df-cbb152bd5ffa",
            "compositeImage": {
                "id": "59e26cfb-7e68-4537-98fa-ce22391c338e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99cdc73e-5176-435a-84b6-7aa6d67ed5b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb2f003c-34bf-4ec6-b396-55a538c72686",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99cdc73e-5176-435a-84b6-7aa6d67ed5b1",
                    "LayerId": "dadd2e6d-9e60-44d4-b746-658f947e3f8a"
                }
            ]
        },
        {
            "id": "699e9043-1f63-4fe7-9f7b-3860c103448c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89754024-d5c6-4252-b2df-cbb152bd5ffa",
            "compositeImage": {
                "id": "9678a7dd-5cb6-4275-bfea-581305f9cd5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "699e9043-1f63-4fe7-9f7b-3860c103448c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1c4716a-09b2-4861-8948-8fe9b3d21bac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "699e9043-1f63-4fe7-9f7b-3860c103448c",
                    "LayerId": "dadd2e6d-9e60-44d4-b746-658f947e3f8a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "dadd2e6d-9e60-44d4-b746-658f947e3f8a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89754024-d5c6-4252-b2df-cbb152bd5ffa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}