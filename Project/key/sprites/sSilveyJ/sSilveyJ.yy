{
    "id": "88f0fe2b-e534-4209-b8e8-73d44c9041a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSilveyJ",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f564e7dd-1633-4f24-af87-32587bdf39e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88f0fe2b-e534-4209-b8e8-73d44c9041a0",
            "compositeImage": {
                "id": "33b354d2-99d2-47e6-b9d3-54daf9aed242",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f564e7dd-1633-4f24-af87-32587bdf39e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "116c6e47-baca-44c9-b56d-86708afdc9bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f564e7dd-1633-4f24-af87-32587bdf39e9",
                    "LayerId": "299799ea-3cfe-4f39-8133-71060346de50"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "299799ea-3cfe-4f39-8133-71060346de50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88f0fe2b-e534-4209-b8e8-73d44c9041a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}