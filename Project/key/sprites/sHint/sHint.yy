{
    "id": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHint",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a57ec72-3494-4018-869d-a76ec1654401",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "0834ec85-ecd4-4d84-951d-64b32183b9b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a57ec72-3494-4018-869d-a76ec1654401",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f4e5a2d-7203-4b69-a99f-0c49a89ff2fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a57ec72-3494-4018-869d-a76ec1654401",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "e3a205ba-ed05-4ffe-ae94-dbd61871aec7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "88070c30-15ae-436a-95f3-db0b0d677ea2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3a205ba-ed05-4ffe-ae94-dbd61871aec7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e42bb307-56a9-40b8-8470-18e1d9f752c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3a205ba-ed05-4ffe-ae94-dbd61871aec7",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "daa38f45-3623-49ad-b34f-b40cd78739f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "03aff7fb-0c36-4ed3-88c2-d1094170c61e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daa38f45-3623-49ad-b34f-b40cd78739f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f9acf7b-a4c9-463a-bee0-d65e391dcc68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daa38f45-3623-49ad-b34f-b40cd78739f9",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "466c5a63-f8d8-4294-8fbd-f08edc27765f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "56b8d99f-7c80-4b78-ab93-75f4d86e1f65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "466c5a63-f8d8-4294-8fbd-f08edc27765f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2454e59c-6367-4ad1-8436-411bc0189cf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "466c5a63-f8d8-4294-8fbd-f08edc27765f",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "78077bc0-72c1-4202-bdba-b0726c0a0093",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "8870ea4d-e99a-4d2d-9939-8930f2348f86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78077bc0-72c1-4202-bdba-b0726c0a0093",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa7c49a9-253f-41f3-9f37-3bf9680ebb2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78077bc0-72c1-4202-bdba-b0726c0a0093",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "c8c97053-2446-494c-a980-8cc74f93faea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "6fcea0cd-3b43-4f75-a58f-3ef1d6128e93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8c97053-2446-494c-a980-8cc74f93faea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ee90387-135a-4c4a-baf2-c7fa3f47dd75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8c97053-2446-494c-a980-8cc74f93faea",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "77697b30-dff5-4b4f-8168-3c3a23904cbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "a95ded16-6580-4379-98e9-47aba3259433",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77697b30-dff5-4b4f-8168-3c3a23904cbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1929e1e6-c467-44a6-8641-f5b429c1edf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77697b30-dff5-4b4f-8168-3c3a23904cbe",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "940a4dea-9266-41c5-8e11-0690bed69114",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "8c55393a-f6d5-42f1-ba5d-66f85cbc5171",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "940a4dea-9266-41c5-8e11-0690bed69114",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3040a013-4257-467d-b093-526faa40ac63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "940a4dea-9266-41c5-8e11-0690bed69114",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "44132298-f37f-4255-8317-7df4d60eb87c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "53b4c4b7-6bcf-4713-9a3d-2131563efd9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44132298-f37f-4255-8317-7df4d60eb87c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14791a85-7c7f-47b2-8707-0628d6f647d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44132298-f37f-4255-8317-7df4d60eb87c",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "6094e072-5be8-4ac6-8a72-a58f2565ed6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "2e6541b5-fbf4-4251-b37e-808647d0bc57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6094e072-5be8-4ac6-8a72-a58f2565ed6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15834c76-df51-402b-8df7-cb8d9088a26d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6094e072-5be8-4ac6-8a72-a58f2565ed6b",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "02aa2cb7-02a5-44c5-b503-5a5626057d5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "8924ec9a-0c30-4130-9d7b-74887161c11d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02aa2cb7-02a5-44c5-b503-5a5626057d5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bc450ca-254a-41c1-b3f5-b4db0c2a8bc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02aa2cb7-02a5-44c5-b503-5a5626057d5a",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "67257e2a-6a3a-44a8-97d1-b6b03f8e967a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "d44737b3-bc24-40a4-b11e-33c61ae17f00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67257e2a-6a3a-44a8-97d1-b6b03f8e967a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d55cec8-d989-4533-a2e2-96a78876246f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67257e2a-6a3a-44a8-97d1-b6b03f8e967a",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "c59f94f8-6990-4ac0-8dcb-d5b84cec5c11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "0291bb8e-d5c6-44f3-973b-2d84a675d5eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c59f94f8-6990-4ac0-8dcb-d5b84cec5c11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff80d8d7-f8c3-4250-a170-92d6c6299bf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c59f94f8-6990-4ac0-8dcb-d5b84cec5c11",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "5b1ec982-ac12-4813-884e-3711af6c5f7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "f58d9d3b-74ec-4365-9968-ed51cd4e6236",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b1ec982-ac12-4813-884e-3711af6c5f7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abd26326-b18b-4873-8d35-d88c80a6cbdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b1ec982-ac12-4813-884e-3711af6c5f7a",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "ba48d4b9-ba31-4404-b438-18cb1ba86c37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "f608680f-662a-4a36-b810-3383c55b091e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba48d4b9-ba31-4404-b438-18cb1ba86c37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ff52216-95c2-4dd1-b239-fac5bb942ae6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba48d4b9-ba31-4404-b438-18cb1ba86c37",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "8af4bf7f-ddca-4771-88f3-dcc784d38ea1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "ac34f9b0-ee21-48c3-92bb-71cec5a0f89a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8af4bf7f-ddca-4771-88f3-dcc784d38ea1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80995204-0cf6-4040-93ef-42c90dc3741b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8af4bf7f-ddca-4771-88f3-dcc784d38ea1",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "8f4a3c1e-a1dc-486e-a180-03cf0823fe25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "2142fb76-083e-4d02-975e-513cdce1b766",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f4a3c1e-a1dc-486e-a180-03cf0823fe25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cceb5ab4-4b04-46b0-b1a7-e5527a0af0ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f4a3c1e-a1dc-486e-a180-03cf0823fe25",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "23a779d8-a6b5-45bf-b5be-62157d14c6a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "885bb3e4-5488-4a65-b17d-5346dfbb639a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23a779d8-a6b5-45bf-b5be-62157d14c6a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3182a53-94f1-4274-8361-a00d018d289f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23a779d8-a6b5-45bf-b5be-62157d14c6a0",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "27a041b5-0300-4323-924a-29979ef18772",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "19b4a484-1522-4b69-be53-19828a9a7ec3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27a041b5-0300-4323-924a-29979ef18772",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "880be70a-9b0e-4f14-a80c-7afe551dc66f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27a041b5-0300-4323-924a-29979ef18772",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "96e70df3-5290-45db-8146-2bab769e31f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "779befd4-0b7f-414e-9a7b-7d2da2ae15ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96e70df3-5290-45db-8146-2bab769e31f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6789fa4-20f9-4a31-b120-a0584d9b6f6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96e70df3-5290-45db-8146-2bab769e31f3",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "084f6cf2-d1c7-410f-be1a-8ac67bd64d76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "bc288d99-45c4-467f-b30d-c0ca294d7950",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "084f6cf2-d1c7-410f-be1a-8ac67bd64d76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "515dad48-f37d-41c8-99e5-edd5eda3d760",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "084f6cf2-d1c7-410f-be1a-8ac67bd64d76",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "1739c0be-40a8-4e72-8c73-ff7963c232d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "53a21241-77f7-4cbb-b52d-9326f50e3908",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1739c0be-40a8-4e72-8c73-ff7963c232d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e39b95d-f71a-428f-83ad-87e2c542578c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1739c0be-40a8-4e72-8c73-ff7963c232d6",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "8b87d798-a947-4d8f-9a7a-04f1d59a7e9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "707d660d-8760-4aa1-ad5d-92b0067f062b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b87d798-a947-4d8f-9a7a-04f1d59a7e9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f228b3d-b582-4692-890e-16f3c98e48bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b87d798-a947-4d8f-9a7a-04f1d59a7e9e",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "d49cfd28-c8ea-4ee8-aad7-4dc1108af414",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "aaac7648-5d3b-4ab4-bbe5-6cc4d1374e0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d49cfd28-c8ea-4ee8-aad7-4dc1108af414",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e2af861-18d9-4084-ac40-5e8da8b60cb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d49cfd28-c8ea-4ee8-aad7-4dc1108af414",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "b2744577-ca8f-46c4-926f-e4e0bb37573e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "d9cb5c07-2d90-4ecf-ab12-0dd7c19b8369",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2744577-ca8f-46c4-926f-e4e0bb37573e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e55751e-95f3-4ea7-946a-fb5033f6784e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2744577-ca8f-46c4-926f-e4e0bb37573e",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "9f5481d0-576d-44d7-93c9-165c72538ba8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "a9b5c285-ea0a-4e7a-9583-68ed0aa53155",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f5481d0-576d-44d7-93c9-165c72538ba8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bf02cb3-b92c-44d9-a037-4d88397ff5ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f5481d0-576d-44d7-93c9-165c72538ba8",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "97a7fabf-0aae-4b15-8dc0-6fbd4fbc8e33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "1b74fe55-9dbb-4833-a5fc-784097820b99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97a7fabf-0aae-4b15-8dc0-6fbd4fbc8e33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c057c2d-7982-4618-b815-73a3103230ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97a7fabf-0aae-4b15-8dc0-6fbd4fbc8e33",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "83af2f8f-3c1c-44e0-bcf4-0b88dd01fc73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "e76c7497-8d13-425c-b07a-258f7c43242a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83af2f8f-3c1c-44e0-bcf4-0b88dd01fc73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "055debb0-7203-4bc0-ab56-9999bdd317a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83af2f8f-3c1c-44e0-bcf4-0b88dd01fc73",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "797e0d7f-618d-4e06-b787-17b74cadc741",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "b3a187d4-f0ec-446e-afae-2b0918236f41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "797e0d7f-618d-4e06-b787-17b74cadc741",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de645b86-6306-4dc7-8eec-7207502761be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "797e0d7f-618d-4e06-b787-17b74cadc741",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "167868aa-06dd-4e6a-ba63-cddbb4213a57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "a9848b97-a9ca-4643-9a0c-6bedf824c425",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "167868aa-06dd-4e6a-ba63-cddbb4213a57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ff465a9-c409-4ce1-a854-a9a3ae63775b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "167868aa-06dd-4e6a-ba63-cddbb4213a57",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "28ca93bd-f556-405b-9cde-a97564d3dbae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "a85c45c5-ec58-437b-9237-830b54f525ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28ca93bd-f556-405b-9cde-a97564d3dbae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d96afa85-4a24-4f4e-98fd-3bfa2eefb5ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28ca93bd-f556-405b-9cde-a97564d3dbae",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        },
        {
            "id": "20aea4d5-ef48-4a1e-9f74-c90b89bf551e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "compositeImage": {
                "id": "f6ce8375-f3c9-44a7-b2c0-902e35a2101d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20aea4d5-ef48-4a1e-9f74-c90b89bf551e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17d1b1a9-9c23-43b3-8065-0c9bcc95c9f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20aea4d5-ef48-4a1e-9f74-c90b89bf551e",
                    "LayerId": "fc7aeb2a-e856-416a-bdc8-58c70f78499c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "fc7aeb2a-e856-416a-bdc8-58c70f78499c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1da0ffd-9b23-456a-b56d-faca29a7771c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}