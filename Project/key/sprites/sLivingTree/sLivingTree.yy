{
    "id": "734bd1b5-7d2e-4499-8b20-e763b471740d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLivingTree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b0800c4-2920-45c5-9ef3-bfa1537e15d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "734bd1b5-7d2e-4499-8b20-e763b471740d",
            "compositeImage": {
                "id": "0e499eae-bfb0-4096-aa63-6e7ccdc5ff80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b0800c4-2920-45c5-9ef3-bfa1537e15d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33b29f08-66d5-4c61-894f-f1e5bfbb2d29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b0800c4-2920-45c5-9ef3-bfa1537e15d9",
                    "LayerId": "3af7e600-11a2-481f-ae3e-a928ba750a04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3af7e600-11a2-481f-ae3e-a928ba750a04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "734bd1b5-7d2e-4499-8b20-e763b471740d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}