{
    "id": "e8c69112-ed85-4d80-8cff-1ac6f54aee1b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMystery",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f48e57b4-29ee-45b9-999c-345972393188",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8c69112-ed85-4d80-8cff-1ac6f54aee1b",
            "compositeImage": {
                "id": "8e22de64-3cbd-44d2-9153-cf125053b4cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f48e57b4-29ee-45b9-999c-345972393188",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da95e57f-a4cf-453c-9c4a-8a0a6bef4138",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f48e57b4-29ee-45b9-999c-345972393188",
                    "LayerId": "bff0f277-6645-49ea-af39-ac607d529bd5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "bff0f277-6645-49ea-af39-ac607d529bd5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e8c69112-ed85-4d80-8cff-1ac6f54aee1b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}