{
    "id": "69752edf-55b3-4059-8ddc-cacca2d054dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLiar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee33d310-bd5e-451c-ab1b-5e74dafc9f9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69752edf-55b3-4059-8ddc-cacca2d054dc",
            "compositeImage": {
                "id": "3b5ca8bd-fb62-46a3-a095-7ee485349f55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee33d310-bd5e-451c-ab1b-5e74dafc9f9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd6cb79f-fc5e-4a4b-88cf-8147bb858e12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee33d310-bd5e-451c-ab1b-5e74dafc9f9d",
                    "LayerId": "7cac2714-4d74-45e0-bff5-045b22fff020"
                }
            ]
        },
        {
            "id": "220e40fc-b39f-40ce-98ac-b152bf3cf170",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69752edf-55b3-4059-8ddc-cacca2d054dc",
            "compositeImage": {
                "id": "07c48e90-d09f-4f5f-86d3-3834dc8bcd05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "220e40fc-b39f-40ce-98ac-b152bf3cf170",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ee4daee-76e1-47bc-bade-2b5d5cad96ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "220e40fc-b39f-40ce-98ac-b152bf3cf170",
                    "LayerId": "7cac2714-4d74-45e0-bff5-045b22fff020"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7cac2714-4d74-45e0-bff5-045b22fff020",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "69752edf-55b3-4059-8ddc-cacca2d054dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}