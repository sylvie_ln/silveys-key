{
    "id": "24d01db3-a0d0-4f9c-af73-526e3341db6f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sB16Fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "695d7f4f-43a8-4de1-8306-42534032135b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24d01db3-a0d0-4f9c-af73-526e3341db6f",
            "compositeImage": {
                "id": "fa105567-6878-41d5-8c2d-e171fd919119",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "695d7f4f-43a8-4de1-8306-42534032135b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4834752a-5cb4-4c9b-9eeb-848a3842e378",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "695d7f4f-43a8-4de1-8306-42534032135b",
                    "LayerId": "712451b3-3eb4-490f-aca8-d2b2cfa91a20"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "712451b3-3eb4-490f-aca8-d2b2cfa91a20",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24d01db3-a0d0-4f9c-af73-526e3341db6f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}