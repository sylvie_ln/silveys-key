{
    "id": "fe98744e-f806-4d6c-a6b7-8f41262ae6a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8e07fd1-bceb-4705-8e01-a48da682104b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe98744e-f806-4d6c-a6b7-8f41262ae6a2",
            "compositeImage": {
                "id": "e9d53c6a-8f2a-45a3-a4c1-0c435c714ba0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8e07fd1-bceb-4705-8e01-a48da682104b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a7c9561-f669-44f6-922a-424cea8d9988",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8e07fd1-bceb-4705-8e01-a48da682104b",
                    "LayerId": "22901298-d59d-4f84-93b6-fdc2f3f15aea"
                }
            ]
        },
        {
            "id": "c3b8d870-2470-4bec-8db9-18f046c1a661",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe98744e-f806-4d6c-a6b7-8f41262ae6a2",
            "compositeImage": {
                "id": "ea12a631-0871-4fb6-97a7-960d292bc298",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3b8d870-2470-4bec-8db9-18f046c1a661",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "820fb16a-b4b7-4e41-b583-137d06cb524a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3b8d870-2470-4bec-8db9-18f046c1a661",
                    "LayerId": "22901298-d59d-4f84-93b6-fdc2f3f15aea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "22901298-d59d-4f84-93b6-fdc2f3f15aea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe98744e-f806-4d6c-a6b7-8f41262ae6a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}