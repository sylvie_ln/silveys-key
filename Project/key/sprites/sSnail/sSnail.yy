{
    "id": "f37e991c-4039-47f7-b7e9-7511452b8c74",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSnail",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38efe7b0-f5ac-4a53-8c19-ccd17cde3dd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f37e991c-4039-47f7-b7e9-7511452b8c74",
            "compositeImage": {
                "id": "874acd85-b2ad-453f-b77e-50097c0ee018",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38efe7b0-f5ac-4a53-8c19-ccd17cde3dd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "409c4d97-86a1-496f-9c6c-ca6ef89740c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38efe7b0-f5ac-4a53-8c19-ccd17cde3dd3",
                    "LayerId": "8356111e-5ddf-4ec0-85d8-e20e0c89abaa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8356111e-5ddf-4ec0-85d8-e20e0c89abaa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f37e991c-4039-47f7-b7e9-7511452b8c74",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}