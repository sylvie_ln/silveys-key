{
    "id": "ac64d2d6-800b-47fe-9daa-6aba0664a16e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFrogey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "719637a7-8cee-44a4-be56-a4e2f94eb8ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac64d2d6-800b-47fe-9daa-6aba0664a16e",
            "compositeImage": {
                "id": "f8c5ac3a-ac9e-4c59-bcb0-55e0878ad65f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "719637a7-8cee-44a4-be56-a4e2f94eb8ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "121d745b-5c61-4c99-95b8-44f22ebd6980",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "719637a7-8cee-44a4-be56-a4e2f94eb8ed",
                    "LayerId": "ee859082-c003-45fa-a838-2c2c340b4cb9"
                }
            ]
        },
        {
            "id": "6fac8417-f0df-42d3-baca-06750a03940a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac64d2d6-800b-47fe-9daa-6aba0664a16e",
            "compositeImage": {
                "id": "c28f396b-53db-4893-bf0d-9d4753f0645d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fac8417-f0df-42d3-baca-06750a03940a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "473655ae-f90c-4cda-8193-5f5c9727f5ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fac8417-f0df-42d3-baca-06750a03940a",
                    "LayerId": "ee859082-c003-45fa-a838-2c2c340b4cb9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ee859082-c003-45fa-a838-2c2c340b4cb9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac64d2d6-800b-47fe-9daa-6aba0664a16e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}