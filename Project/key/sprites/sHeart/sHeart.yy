{
    "id": "51bb196c-bb8d-4fe1-8bbc-957c6e344cb6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHeart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6bbfef40-7acb-42c8-9c00-033c42d964c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51bb196c-bb8d-4fe1-8bbc-957c6e344cb6",
            "compositeImage": {
                "id": "8d9089c0-9b96-4f1f-a678-3ef7c9037aa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bbfef40-7acb-42c8-9c00-033c42d964c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca5e18b5-5a15-4d7f-bb74-90dddde2511a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bbfef40-7acb-42c8-9c00-033c42d964c0",
                    "LayerId": "e228bf53-38c5-4ecf-a969-bba47da67d09"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e228bf53-38c5-4ecf-a969-bba47da67d09",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51bb196c-bb8d-4fe1-8bbc-957c6e344cb6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}