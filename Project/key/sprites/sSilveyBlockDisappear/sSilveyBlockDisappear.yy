{
    "id": "9bc09227-408b-44d3-80d2-25c51c260c5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSilveyBlockDisappear",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8bee8a7c-d145-402b-afbb-8c1c44142d90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bc09227-408b-44d3-80d2-25c51c260c5c",
            "compositeImage": {
                "id": "af834ca3-6508-4fb5-a646-fb35ec0f4f00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bee8a7c-d145-402b-afbb-8c1c44142d90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16272db6-b5b3-42f8-8da1-603ffa8e198c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bee8a7c-d145-402b-afbb-8c1c44142d90",
                    "LayerId": "699bd897-c76a-42ac-b1d0-c045acdc625e"
                }
            ]
        },
        {
            "id": "fbdd9bad-8168-493b-8569-b4be1535daf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bc09227-408b-44d3-80d2-25c51c260c5c",
            "compositeImage": {
                "id": "f9a4fe36-1ee5-458e-be7f-b7ccbbc88c00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbdd9bad-8168-493b-8569-b4be1535daf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62157b5e-456b-43c4-aa94-b465142cdc22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbdd9bad-8168-493b-8569-b4be1535daf1",
                    "LayerId": "699bd897-c76a-42ac-b1d0-c045acdc625e"
                }
            ]
        },
        {
            "id": "46a124d7-bd6a-40ea-a05e-be00cb1edbf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bc09227-408b-44d3-80d2-25c51c260c5c",
            "compositeImage": {
                "id": "976bd972-2da9-4dd5-b213-0902ba5c9bba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46a124d7-bd6a-40ea-a05e-be00cb1edbf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c329f2aa-d11c-49ce-ba45-c3d0ec93cd62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46a124d7-bd6a-40ea-a05e-be00cb1edbf9",
                    "LayerId": "699bd897-c76a-42ac-b1d0-c045acdc625e"
                }
            ]
        },
        {
            "id": "c29a09a6-3a41-4994-b888-5acccc3d3f23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bc09227-408b-44d3-80d2-25c51c260c5c",
            "compositeImage": {
                "id": "9d80878d-1b25-43bc-9ee9-9fb3fa615c88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c29a09a6-3a41-4994-b888-5acccc3d3f23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6911f7b-9d09-4116-a7bc-8941fa499115",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c29a09a6-3a41-4994-b888-5acccc3d3f23",
                    "LayerId": "699bd897-c76a-42ac-b1d0-c045acdc625e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "699bd897-c76a-42ac-b1d0-c045acdc625e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9bc09227-408b-44d3-80d2-25c51c260c5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}