{
    "id": "a2e2790b-2329-4a3f-86ff-0f68d6507245",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sB16Ending",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6ded3fd-5408-47ce-b921-b077a1335dba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2e2790b-2329-4a3f-86ff-0f68d6507245",
            "compositeImage": {
                "id": "3b49ee4f-ca57-49ac-844f-9a8e59fada23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6ded3fd-5408-47ce-b921-b077a1335dba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f528140c-d5f7-46d9-a648-266fddc8df28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6ded3fd-5408-47ce-b921-b077a1335dba",
                    "LayerId": "46de71af-1635-4439-a2f4-881395ea3e17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "46de71af-1635-4439-a2f4-881395ea3e17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2e2790b-2329-4a3f-86ff-0f68d6507245",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}