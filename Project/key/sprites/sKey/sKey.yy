{
    "id": "ebafa70e-e6dc-463c-b3df-302e9a954c27",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 4,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "937bef69-e3c2-4217-b47f-219f470bb81a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebafa70e-e6dc-463c-b3df-302e9a954c27",
            "compositeImage": {
                "id": "671b3aa7-4e4a-4d71-a54b-1bcd945bde50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "937bef69-e3c2-4217-b47f-219f470bb81a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "123a041c-4fe4-445b-ba40-35fdcfce55af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "937bef69-e3c2-4217-b47f-219f470bb81a",
                    "LayerId": "67d6bcd7-4868-4acd-a332-c78a076e4da1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "67d6bcd7-4868-4acd-a332-c78a076e4da1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ebafa70e-e6dc-463c-b3df-302e9a954c27",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}