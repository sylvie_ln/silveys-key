{
    "id": "b55f2571-0351-4a1c-9b98-8e9fa7cceab2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSilveyW",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b708f672-2550-4059-a932-f44c640e46f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b55f2571-0351-4a1c-9b98-8e9fa7cceab2",
            "compositeImage": {
                "id": "b0ab694b-f886-416c-a8c0-91bf3a3d7dd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b708f672-2550-4059-a932-f44c640e46f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f327cc5e-d6f8-4b9a-958e-ebd7b1052729",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b708f672-2550-4059-a932-f44c640e46f9",
                    "LayerId": "5f71ac63-d106-4078-aed7-5208a0009e76"
                }
            ]
        },
        {
            "id": "147de825-573e-42d0-9daf-9a11b6c1f0e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b55f2571-0351-4a1c-9b98-8e9fa7cceab2",
            "compositeImage": {
                "id": "2e011695-9b72-46e1-b5d2-60c97c515174",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "147de825-573e-42d0-9daf-9a11b6c1f0e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c499fadb-1f30-4d27-b08a-68de4369e642",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "147de825-573e-42d0-9daf-9a11b6c1f0e7",
                    "LayerId": "5f71ac63-d106-4078-aed7-5208a0009e76"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5f71ac63-d106-4078-aed7-5208a0009e76",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b55f2571-0351-4a1c-9b98-8e9fa7cceab2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}