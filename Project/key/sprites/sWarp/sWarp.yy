{
    "id": "e63f24c0-62da-489a-87f8-801f419dc8e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWarp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2bc54892-5f2d-4f4f-ac75-c0fe2753b3f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e63f24c0-62da-489a-87f8-801f419dc8e7",
            "compositeImage": {
                "id": "ae41e94a-7f3a-44f5-acde-ede4149b06a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bc54892-5f2d-4f4f-ac75-c0fe2753b3f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f1db7dc-f426-4bac-9ae8-9e5b064b6bc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bc54892-5f2d-4f4f-ac75-c0fe2753b3f2",
                    "LayerId": "ad06cef0-c670-4233-afa9-bd5c6f63b8c0"
                }
            ]
        },
        {
            "id": "c5a36f90-cb1f-4434-973c-636d0ce82803",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e63f24c0-62da-489a-87f8-801f419dc8e7",
            "compositeImage": {
                "id": "d41e72fb-cce3-44d7-a036-82cd501353e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5a36f90-cb1f-4434-973c-636d0ce82803",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dd4ce5d-02b4-4393-8dbb-740a244b9386",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5a36f90-cb1f-4434-973c-636d0ce82803",
                    "LayerId": "ad06cef0-c670-4233-afa9-bd5c6f63b8c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ad06cef0-c670-4233-afa9-bd5c6f63b8c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e63f24c0-62da-489a-87f8-801f419dc8e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}