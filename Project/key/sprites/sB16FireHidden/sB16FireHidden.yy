{
    "id": "e8b2002e-55ad-44c2-b4c3-88b610dbed7b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sB16FireHidden",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b05f1417-25f1-4b60-b6dd-ca83561ab99f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8b2002e-55ad-44c2-b4c3-88b610dbed7b",
            "compositeImage": {
                "id": "5aa2d5d8-cb7e-4a30-a595-5a1027157784",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b05f1417-25f1-4b60-b6dd-ca83561ab99f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "905119ee-341e-4499-9501-bbed0012a3c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b05f1417-25f1-4b60-b6dd-ca83561ab99f",
                    "LayerId": "9a24f3eb-ea8f-447d-a4a3-c2156d59bfea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9a24f3eb-ea8f-447d-a4a3-c2156d59bfea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e8b2002e-55ad-44c2-b4c3-88b610dbed7b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}