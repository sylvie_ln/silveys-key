{
    "id": "f3ef36b3-acee-4a95-918e-504da30807b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSilveyDB",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc9ac105-4400-4bb1-9472-15881b97bac9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3ef36b3-acee-4a95-918e-504da30807b5",
            "compositeImage": {
                "id": "5383b36e-44b6-484d-bc1e-93f2fb30f65c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc9ac105-4400-4bb1-9472-15881b97bac9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5521f7e-092e-42a0-983c-bb5f553aae34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc9ac105-4400-4bb1-9472-15881b97bac9",
                    "LayerId": "53a1c0be-611a-4119-aea2-c99f36c97f13"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "53a1c0be-611a-4119-aea2-c99f36c97f13",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3ef36b3-acee-4a95-918e-504da30807b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}