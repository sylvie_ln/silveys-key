{
    "id": "2ca87bdf-fc34-4415-a9f4-7a791f069513",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sChess",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "abd46e07-0225-4570-86cb-69bbd1700004",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ca87bdf-fc34-4415-a9f4-7a791f069513",
            "compositeImage": {
                "id": "affcbe83-b0bf-4836-9bfc-766cc5bcdaff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abd46e07-0225-4570-86cb-69bbd1700004",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "941d3b45-887a-48b1-bcc3-3f89928ee73b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abd46e07-0225-4570-86cb-69bbd1700004",
                    "LayerId": "955b4d8d-6469-48d9-98f3-23495186be72"
                }
            ]
        },
        {
            "id": "88eccd88-08e5-4ba4-bb5a-8a91f6704c84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ca87bdf-fc34-4415-a9f4-7a791f069513",
            "compositeImage": {
                "id": "67182f6f-b638-4433-a28d-4ff27fc288df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88eccd88-08e5-4ba4-bb5a-8a91f6704c84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68cf1632-c6d5-4e81-9f6f-32290cb2bb31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88eccd88-08e5-4ba4-bb5a-8a91f6704c84",
                    "LayerId": "955b4d8d-6469-48d9-98f3-23495186be72"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "955b4d8d-6469-48d9-98f3-23495186be72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ca87bdf-fc34-4415-a9f4-7a791f069513",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}