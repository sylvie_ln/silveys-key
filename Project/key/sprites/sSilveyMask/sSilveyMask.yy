{
    "id": "6f3ffb08-5b8a-4fb0-b3db-bc9ca318521a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSilveyMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b595a703-747d-4aa7-8d7a-02385c838508",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f3ffb08-5b8a-4fb0-b3db-bc9ca318521a",
            "compositeImage": {
                "id": "d1049555-c3da-444f-a891-51563435007b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b595a703-747d-4aa7-8d7a-02385c838508",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfc99164-4fd4-4305-bba5-a67e1a8b553a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b595a703-747d-4aa7-8d7a-02385c838508",
                    "LayerId": "ff1e3b7e-4e8d-4128-88aa-d8f57bb3eca6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ff1e3b7e-4e8d-4128-88aa-d8f57bb3eca6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f3ffb08-5b8a-4fb0-b3db-bc9ca318521a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}