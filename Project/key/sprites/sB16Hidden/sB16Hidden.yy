{
    "id": "add96877-8835-4728-9dae-8d4754332389",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sB16Hidden",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c86e2991-07c1-4d5a-8980-cd566a6d423e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "add96877-8835-4728-9dae-8d4754332389",
            "compositeImage": {
                "id": "66b7067b-da8d-41ad-b58a-51ecf7d34ac6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c86e2991-07c1-4d5a-8980-cd566a6d423e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "091502a4-db61-486c-befd-58603789d878",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c86e2991-07c1-4d5a-8980-cd566a6d423e",
                    "LayerId": "1e23d5dc-8917-4214-991e-83de59712332"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1e23d5dc-8917-4214-991e-83de59712332",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "add96877-8835-4728-9dae-8d4754332389",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}