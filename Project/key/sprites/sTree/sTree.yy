{
    "id": "a7b2b18e-7f05-43ae-9576-70cb225cd508",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2fadadb-b292-4e48-ad37-3139a8fc59ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7b2b18e-7f05-43ae-9576-70cb225cd508",
            "compositeImage": {
                "id": "849693fb-b904-43a6-b102-4e314d1ee68e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2fadadb-b292-4e48-ad37-3139a8fc59ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "761c1a35-d011-4726-aa18-e3a3bcc87690",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2fadadb-b292-4e48-ad37-3139a8fc59ec",
                    "LayerId": "d885f8da-8b6c-444b-bdbe-dceb5d98cf00"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d885f8da-8b6c-444b-bdbe-dceb5d98cf00",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7b2b18e-7f05-43ae-9576-70cb225cd508",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}