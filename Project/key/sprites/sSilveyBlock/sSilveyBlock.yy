{
    "id": "f5963390-0e9b-408f-97c6-434f2a81f68b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSilveyBlock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c27bc6d1-c964-47a2-8775-7b233df92dfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5963390-0e9b-408f-97c6-434f2a81f68b",
            "compositeImage": {
                "id": "3416f852-0536-4855-836a-41ff69d9ad60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c27bc6d1-c964-47a2-8775-7b233df92dfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67c52930-b56e-40e1-b379-aab846dc5060",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c27bc6d1-c964-47a2-8775-7b233df92dfd",
                    "LayerId": "f4cd7ce2-6196-4fd9-9039-f26afc41e488"
                }
            ]
        },
        {
            "id": "cd348f10-1752-4dae-b4ef-60e9fc3b42cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5963390-0e9b-408f-97c6-434f2a81f68b",
            "compositeImage": {
                "id": "0d94b876-9842-4fd4-b03e-bda39e0492c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd348f10-1752-4dae-b4ef-60e9fc3b42cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc15e998-e788-4a7d-a360-8bbf5d22f397",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd348f10-1752-4dae-b4ef-60e9fc3b42cb",
                    "LayerId": "f4cd7ce2-6196-4fd9-9039-f26afc41e488"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f4cd7ce2-6196-4fd9-9039-f26afc41e488",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5963390-0e9b-408f-97c6-434f2a81f68b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}