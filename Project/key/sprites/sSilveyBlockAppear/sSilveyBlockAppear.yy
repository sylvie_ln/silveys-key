{
    "id": "1bddf983-5ecf-44e4-b7f8-87dadae7d1ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSilveyBlockAppear",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d7a4ae9c-aae7-4ced-bafa-5b0fa37c42c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bddf983-5ecf-44e4-b7f8-87dadae7d1ad",
            "compositeImage": {
                "id": "af1b7040-d7c9-45b7-94f2-1820870fb22d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7a4ae9c-aae7-4ced-bafa-5b0fa37c42c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87e797de-c0c3-42ef-bbeb-150c2c60bd1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7a4ae9c-aae7-4ced-bafa-5b0fa37c42c3",
                    "LayerId": "e0e4b840-0389-4269-8068-dda792201bd7"
                }
            ]
        },
        {
            "id": "b2adc240-ba4c-4cc2-b258-7c8d172895ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bddf983-5ecf-44e4-b7f8-87dadae7d1ad",
            "compositeImage": {
                "id": "fc3695a5-30d6-454b-817f-d6a3ee6461d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2adc240-ba4c-4cc2-b258-7c8d172895ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "132a8869-9dd5-4768-93ee-bcbb978c4493",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2adc240-ba4c-4cc2-b258-7c8d172895ce",
                    "LayerId": "e0e4b840-0389-4269-8068-dda792201bd7"
                }
            ]
        },
        {
            "id": "62e4ad4f-517e-4d1f-91aa-d838b75ea3fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bddf983-5ecf-44e4-b7f8-87dadae7d1ad",
            "compositeImage": {
                "id": "5ebe31c2-8969-4dac-9206-cc246d3a3d88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62e4ad4f-517e-4d1f-91aa-d838b75ea3fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb88d654-e377-41f4-89eb-23b37619dc06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62e4ad4f-517e-4d1f-91aa-d838b75ea3fa",
                    "LayerId": "e0e4b840-0389-4269-8068-dda792201bd7"
                }
            ]
        },
        {
            "id": "ca0d90e8-98b8-49b9-98dc-9ace1ec83fb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bddf983-5ecf-44e4-b7f8-87dadae7d1ad",
            "compositeImage": {
                "id": "f64927f9-c28a-4284-914d-eb8a4ca89228",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca0d90e8-98b8-49b9-98dc-9ace1ec83fb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e6e200b-2d33-405e-83f6-e85c5278a98b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca0d90e8-98b8-49b9-98dc-9ace1ec83fb1",
                    "LayerId": "e0e4b840-0389-4269-8068-dda792201bd7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e0e4b840-0389-4269-8068-dda792201bd7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1bddf983-5ecf-44e4-b7f8-87dadae7d1ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}