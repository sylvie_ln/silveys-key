{
    "id": "062f3192-32b9-47d9-aa4e-1e9e95cb0c53",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpotlightBottom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 96,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "506bc81a-6358-4b9b-8ea7-478d7b7093c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "062f3192-32b9-47d9-aa4e-1e9e95cb0c53",
            "compositeImage": {
                "id": "644b26e4-263e-48ad-9699-eb8151059f3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "506bc81a-6358-4b9b-8ea7-478d7b7093c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0e0f5b3-0586-4d7f-9a92-c4d6c0d2b7ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "506bc81a-6358-4b9b-8ea7-478d7b7093c2",
                    "LayerId": "de6abdda-b766-4e60-95b6-ea0d9b129e82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "de6abdda-b766-4e60-95b6-ea0d9b129e82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "062f3192-32b9-47d9-aa4e-1e9e95cb0c53",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 112
}