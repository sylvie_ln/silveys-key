{
    "id": "f81d8223-a61c-499a-b92a-33ad570dce93",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHintBW",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8dce7c0-eb69-4455-ba47-1abdfb0f732e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "3e60cf09-f5a4-4464-a427-725cac17b96a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8dce7c0-eb69-4455-ba47-1abdfb0f732e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5909358c-670f-4d01-b398-a76682137f14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8dce7c0-eb69-4455-ba47-1abdfb0f732e",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "3b9ba82d-c051-43c2-861f-ed6262a0bbcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "be8c918e-5836-4dc9-a9fa-e506786d7d6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b9ba82d-c051-43c2-861f-ed6262a0bbcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fa45ebf-e132-4d1f-89f4-c12043cbf192",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b9ba82d-c051-43c2-861f-ed6262a0bbcc",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "6be4b34c-1ca2-4a14-b948-d2f97d11b9dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "4c18db1e-ce64-40d8-8503-c74bf3f2af46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6be4b34c-1ca2-4a14-b948-d2f97d11b9dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f497af76-b822-48b0-a4cc-be7b9f0b7cde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6be4b34c-1ca2-4a14-b948-d2f97d11b9dd",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "a5fd57da-f152-4e77-92f7-0675d4cc2f13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "82d20d99-b808-44d4-a679-74bad3eb70cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5fd57da-f152-4e77-92f7-0675d4cc2f13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07043c04-e753-47b6-8814-5d1ca226bd8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5fd57da-f152-4e77-92f7-0675d4cc2f13",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "40cbc0b3-ec2b-4dd1-bddd-94b12455155e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "5863c837-541c-4647-b718-323532b471d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40cbc0b3-ec2b-4dd1-bddd-94b12455155e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad51f6dd-601d-4a08-afdb-4f9ee8d4b381",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40cbc0b3-ec2b-4dd1-bddd-94b12455155e",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "30e437e7-8bff-4ab8-b957-e0777544f22c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "885011d6-044c-4384-816f-497d497140cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30e437e7-8bff-4ab8-b957-e0777544f22c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d282d0a8-44c9-4ade-955b-a4710b2a420d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30e437e7-8bff-4ab8-b957-e0777544f22c",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "be0f8595-2c9b-46ef-8bc2-266aba22c1b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "1ec03d26-457f-4999-801b-e0a8e6f96b94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be0f8595-2c9b-46ef-8bc2-266aba22c1b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c535cbb-e49a-40f9-a89b-8fdff112b911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be0f8595-2c9b-46ef-8bc2-266aba22c1b6",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "3bbfc0d1-c8a5-4041-89b9-be2c9132b41c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "b36f9735-487b-4ba7-b718-042bb3ca1ad9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bbfc0d1-c8a5-4041-89b9-be2c9132b41c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "003f976c-7360-40cc-9499-a13eefc78343",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bbfc0d1-c8a5-4041-89b9-be2c9132b41c",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "6cb1d3df-ce90-494e-9789-361d43c83c11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "407a1e26-e9f7-40da-8d8a-7f86cfbbbaec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cb1d3df-ce90-494e-9789-361d43c83c11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e27bbf5e-2403-4e30-b346-b1e2bc252039",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cb1d3df-ce90-494e-9789-361d43c83c11",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "0ea1c312-6cc1-4634-b688-6da327cc32fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "3026fa8a-c0d9-45bf-a0a3-bd77d9c97c78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ea1c312-6cc1-4634-b688-6da327cc32fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9f282eb-1767-402e-8edd-5bc498f7094c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ea1c312-6cc1-4634-b688-6da327cc32fb",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "196cf9f8-32c4-47a7-bb67-e1af24bc79d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "54b7379b-2d81-4682-ae99-f4300a92c0e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "196cf9f8-32c4-47a7-bb67-e1af24bc79d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d68b227a-2178-4050-a18e-2055e38ac35a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "196cf9f8-32c4-47a7-bb67-e1af24bc79d2",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "1fe4bc23-af24-443d-a782-7741894b98c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "09f0860b-e93e-4488-9a12-befa6de9bd5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fe4bc23-af24-443d-a782-7741894b98c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdfc2bb5-be39-4ecc-bb0d-c4d3913d8529",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fe4bc23-af24-443d-a782-7741894b98c9",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "3248f729-143a-4c40-ae73-1573ba7e7d59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "46e4ff27-ef36-4f54-8d9c-d18b9bcc9d8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3248f729-143a-4c40-ae73-1573ba7e7d59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7a9fac5-71cf-4bfd-b83c-708f09accb8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3248f729-143a-4c40-ae73-1573ba7e7d59",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "99abd17e-9e57-46d1-84d8-ad1cdf4e7610",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "5565ed50-4321-42f9-b133-1ad3a9fd886b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99abd17e-9e57-46d1-84d8-ad1cdf4e7610",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcf5fa84-2a45-4b0b-879f-3c10a4d8069e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99abd17e-9e57-46d1-84d8-ad1cdf4e7610",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "ee1597b5-a2a2-43c5-addc-c79a73114929",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "f0a9498d-d8a0-4282-b5d4-a7cc503425ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee1597b5-a2a2-43c5-addc-c79a73114929",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9df3fc26-4084-4bb0-ba0a-feff6bac36b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee1597b5-a2a2-43c5-addc-c79a73114929",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "f1c26525-4974-433d-93bb-93fbc64e61cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "dfb68839-5ab3-44ac-a397-58492b1a0589",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1c26525-4974-433d-93bb-93fbc64e61cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99f785c6-4131-4e5e-b6aa-140313e2c91e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1c26525-4974-433d-93bb-93fbc64e61cc",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "84c64b80-1a7c-4a98-bc7d-09d22083cbec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "55b844b8-a175-4062-af66-8f4f7325caec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84c64b80-1a7c-4a98-bc7d-09d22083cbec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f60c91c1-5d02-4c85-861e-ea5ffa7dca37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84c64b80-1a7c-4a98-bc7d-09d22083cbec",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "a159c6dd-ea66-4a34-81e2-e25cfa2636b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "3fed5639-c32e-41dd-9a9a-af978a0633d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a159c6dd-ea66-4a34-81e2-e25cfa2636b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70fa4ca7-57bf-4fab-b842-3e6b2754850c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a159c6dd-ea66-4a34-81e2-e25cfa2636b0",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "c296d723-29d6-4063-a9ac-980295c9a6e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "06d7cb76-20af-4c1a-bfd5-dede57bb36b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c296d723-29d6-4063-a9ac-980295c9a6e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f17ff430-2f2a-44f7-b8af-afaef30b4053",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c296d723-29d6-4063-a9ac-980295c9a6e3",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "ded9015d-751c-4f1f-8f89-d77cbe7c6e56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "952fe6e5-86e5-4c7f-93bb-39382943e2c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ded9015d-751c-4f1f-8f89-d77cbe7c6e56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aead3557-056d-415c-b35a-eb4e6ddf1feb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ded9015d-751c-4f1f-8f89-d77cbe7c6e56",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "1c51b606-12c0-4763-8f9f-248f08273d42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "7e7b5cf0-9d92-4e61-be61-422fa0dd1527",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c51b606-12c0-4763-8f9f-248f08273d42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e691fec4-29e1-47b7-95a2-2498658d9f30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c51b606-12c0-4763-8f9f-248f08273d42",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "3930d859-2710-413c-a91c-771fa20508f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "5d583fcd-0e96-41c3-ba08-d7372064c210",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3930d859-2710-413c-a91c-771fa20508f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9467e9f-fab6-41bd-8d9b-423fea9eac2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3930d859-2710-413c-a91c-771fa20508f0",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "af4b84be-fffa-463d-8708-41f4ad35e2a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "712b5455-2b91-4853-afdb-e8fa52bd94d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af4b84be-fffa-463d-8708-41f4ad35e2a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec56119d-df95-4114-958d-81a8fc772e29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af4b84be-fffa-463d-8708-41f4ad35e2a1",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "aca74a6c-2f67-4e54-a76c-9f5f1ce82e81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "320d71ea-9a92-4bea-b400-7bb78e4d0cb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aca74a6c-2f67-4e54-a76c-9f5f1ce82e81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01da5b85-51d3-4551-ade6-d4ddc7f2d5e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aca74a6c-2f67-4e54-a76c-9f5f1ce82e81",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "f26384fd-967d-4edc-ae7c-368b983e51aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "c44a315e-d816-4a51-9130-e71fa3978ed2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f26384fd-967d-4edc-ae7c-368b983e51aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b381396a-9f2b-4d09-b199-938172e93a5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f26384fd-967d-4edc-ae7c-368b983e51aa",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "cf9a52ba-8c9c-4e54-8308-6e968cf7a034",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "ff366170-6904-4d3f-bc91-fd71e2ab0736",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf9a52ba-8c9c-4e54-8308-6e968cf7a034",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9995cdce-71e7-44b3-b7f2-e96e7650412f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf9a52ba-8c9c-4e54-8308-6e968cf7a034",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "8d6e96bc-4377-43ae-9c64-f0ee614edeaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "3ae458f6-af6c-45cb-b10c-56688e3269a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d6e96bc-4377-43ae-9c64-f0ee614edeaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f7460b8-5e6c-4c8a-8c78-e4ab43ec49e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d6e96bc-4377-43ae-9c64-f0ee614edeaf",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "6ebb4969-9af4-44fe-bfa9-5ae9b6963ce3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "d472bdd6-0f06-4a43-8634-01291c90ee7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ebb4969-9af4-44fe-bfa9-5ae9b6963ce3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67cda64c-da8d-4ef2-9b50-d8320c8e2ae3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ebb4969-9af4-44fe-bfa9-5ae9b6963ce3",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "118bc63c-f66b-479d-a3bb-4bf439a02fd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "a85a8367-d858-4ff4-944f-c3eebf2ac1ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "118bc63c-f66b-479d-a3bb-4bf439a02fd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb25c3ac-909d-49b1-aa5e-93c4fdfec6ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "118bc63c-f66b-479d-a3bb-4bf439a02fd3",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "313c5cc2-e382-4188-8728-890e09b842f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "674ae42b-6d91-4528-b8ec-57039bfb1823",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "313c5cc2-e382-4188-8728-890e09b842f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57e93298-8337-46d1-9aa0-290f1e30fcaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "313c5cc2-e382-4188-8728-890e09b842f9",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "a5adfdd5-ca36-4db7-bf01-006b314397d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "44b0249c-8429-4936-9a32-6cfd3e9e6049",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5adfdd5-ca36-4db7-bf01-006b314397d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f44620f2-b7ce-46e5-9bf9-8f0ee6f0ba40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5adfdd5-ca36-4db7-bf01-006b314397d1",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "c6c68c47-4248-4462-8700-fb43f726ea5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "13d9d89a-8925-4777-ba9b-89a7b08da4b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6c68c47-4248-4462-8700-fb43f726ea5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1202b6ff-1245-4119-ba22-a52e286a296c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6c68c47-4248-4462-8700-fb43f726ea5f",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "bffeb4e6-0ae5-43ee-871f-b2fe588e15de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "7f8adcc2-a4d2-4a85-b15b-3dd28f1a1b18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bffeb4e6-0ae5-43ee-871f-b2fe588e15de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5426332f-9066-434c-812b-5ed6d313c8c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bffeb4e6-0ae5-43ee-871f-b2fe588e15de",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "ef27c00c-843d-40ee-b373-d52e631a18ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "146bde92-dc8e-4930-ad43-fde0373b2691",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef27c00c-843d-40ee-b373-d52e631a18ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b30fb9e-e6a4-424a-9c8f-380dca8c1106",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef27c00c-843d-40ee-b373-d52e631a18ec",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "ff46893b-8e34-4114-911e-a3d7a042bf9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "8faf0bd4-5390-4205-86e4-547d4fbe1a3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff46893b-8e34-4114-911e-a3d7a042bf9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c222009-1977-4747-a259-1168ab326b7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff46893b-8e34-4114-911e-a3d7a042bf9d",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        },
        {
            "id": "01abf455-daa0-476a-aff3-d90c9ec62615",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "compositeImage": {
                "id": "d329ab68-7f33-48e4-b5b8-494246075181",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01abf455-daa0-476a-aff3-d90c9ec62615",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51e1124a-9bea-43d8-ad1d-6ab5be08e4eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01abf455-daa0-476a-aff3-d90c9ec62615",
                    "LayerId": "c9c36676-f265-48df-808b-b23071e83e86"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c9c36676-f265-48df-808b-b23071e83e86",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f81d8223-a61c-499a-b92a-33ad570dce93",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}