{
    "id": "483543ed-99b7-4c78-82e0-9a9da7f6145e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSilveyF",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b144592-cfad-43c5-8b28-de5d75626149",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "483543ed-99b7-4c78-82e0-9a9da7f6145e",
            "compositeImage": {
                "id": "08a40844-68b4-4c4f-bcd2-9d5d215490e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b144592-cfad-43c5-8b28-de5d75626149",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b8919c8-1bc2-4d67-ad25-ea96cdfccd49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b144592-cfad-43c5-8b28-de5d75626149",
                    "LayerId": "5cf2f240-882b-456f-9a3a-17ff22170cef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5cf2f240-882b-456f-9a3a-17ff22170cef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "483543ed-99b7-4c78-82e0-9a9da7f6145e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}