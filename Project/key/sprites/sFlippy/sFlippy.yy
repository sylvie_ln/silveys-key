{
    "id": "ecbdec58-0d0c-4eb4-a944-32a1313c96cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFlippy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b90d66f5-4240-4241-abcc-ca6cbf0bbbed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecbdec58-0d0c-4eb4-a944-32a1313c96cc",
            "compositeImage": {
                "id": "9abf719c-5d8a-4cc0-9600-c0cda703d7d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b90d66f5-4240-4241-abcc-ca6cbf0bbbed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11d1ca6b-9695-4876-9f1d-ad98853a2d09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b90d66f5-4240-4241-abcc-ca6cbf0bbbed",
                    "LayerId": "ec1fa313-f3e7-4c19-be7c-06277d0906e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ec1fa313-f3e7-4c19-be7c-06277d0906e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ecbdec58-0d0c-4eb4-a944-32a1313c96cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}