{
    "id": "d3563eb5-08e8-499f-a686-23da78f9e0f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpotlight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 108,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "229bf755-28de-4998-b1b8-230d6841ba36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3563eb5-08e8-499f-a686-23da78f9e0f8",
            "compositeImage": {
                "id": "32857eab-be4d-4199-b810-27c4d86b5724",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "229bf755-28de-4998-b1b8-230d6841ba36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "767c294d-050e-4090-b3a7-0c6475dc32dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "229bf755-28de-4998-b1b8-230d6841ba36",
                    "LayerId": "44703c66-267f-4433-a1b7-107b41cf034f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "44703c66-267f-4433-a1b7-107b41cf034f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3563eb5-08e8-499f-a686-23da78f9e0f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 112
}