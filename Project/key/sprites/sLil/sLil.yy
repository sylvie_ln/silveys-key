{
    "id": "0215d2db-cdbf-481a-bd2a-51b72a48cfbb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLil",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3cc37aa8-b5f4-4d0c-aa19-23ebdcc9c16c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0215d2db-cdbf-481a-bd2a-51b72a48cfbb",
            "compositeImage": {
                "id": "5c53b606-8fd2-4eca-820e-acb8dfeac1a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cc37aa8-b5f4-4d0c-aa19-23ebdcc9c16c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a552317c-e5e5-4aec-a97c-c0c225d7adbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cc37aa8-b5f4-4d0c-aa19-23ebdcc9c16c",
                    "LayerId": "0fb05609-a407-4636-ab0b-0b7af2b67edf"
                }
            ]
        },
        {
            "id": "a167768d-ec09-4a83-8a9e-9091497552d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0215d2db-cdbf-481a-bd2a-51b72a48cfbb",
            "compositeImage": {
                "id": "03d77d47-a5a7-49c9-9fb3-956b7afbc5fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a167768d-ec09-4a83-8a9e-9091497552d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fd415c8-af90-4bd9-a345-329598d42bd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a167768d-ec09-4a83-8a9e-9091497552d5",
                    "LayerId": "0fb05609-a407-4636-ab0b-0b7af2b67edf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0fb05609-a407-4636-ab0b-0b7af2b67edf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0215d2db-cdbf-481a-bd2a-51b72a48cfbb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}