{
    "id": "b99fc400-6af2-400b-9f5c-562c56bcd590",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCola",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 3,
    "bbox_right": 11,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc66cc2b-05c6-48d3-9525-5aae53d6f35b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b99fc400-6af2-400b-9f5c-562c56bcd590",
            "compositeImage": {
                "id": "c37195af-e0ea-4e63-89d0-b73a6ce60427",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc66cc2b-05c6-48d3-9525-5aae53d6f35b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93e2a3f0-172c-4c15-b31c-f661d59c51bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc66cc2b-05c6-48d3-9525-5aae53d6f35b",
                    "LayerId": "1e5b6013-7e69-46af-b044-d4fcb1b0a823"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1e5b6013-7e69-46af-b044-d4fcb1b0a823",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b99fc400-6af2-400b-9f5c-562c56bcd590",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}