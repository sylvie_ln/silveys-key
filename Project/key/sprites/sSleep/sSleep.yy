{
    "id": "8aa075b9-f0ba-4a5f-ae9d-a9e16876def3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSleep",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f633128-2e79-429e-a27e-092d5bb046bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8aa075b9-f0ba-4a5f-ae9d-a9e16876def3",
            "compositeImage": {
                "id": "b7e92221-8b97-41a4-b403-060a062273a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f633128-2e79-429e-a27e-092d5bb046bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc80d365-3a8e-47be-9362-54e23a1e74b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f633128-2e79-429e-a27e-092d5bb046bd",
                    "LayerId": "8e791faa-c19f-4e54-818e-c35a6b3befeb"
                }
            ]
        },
        {
            "id": "df41bb31-18d1-4c78-b223-e60a59068726",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8aa075b9-f0ba-4a5f-ae9d-a9e16876def3",
            "compositeImage": {
                "id": "463f9f57-5326-4429-82bb-4e88810b032d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df41bb31-18d1-4c78-b223-e60a59068726",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e62b83e-b347-4582-87aa-16d84d9e0f43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df41bb31-18d1-4c78-b223-e60a59068726",
                    "LayerId": "8e791faa-c19f-4e54-818e-c35a6b3befeb"
                }
            ]
        },
        {
            "id": "3dacd9aa-8c13-4fcf-8497-366cadf9e5f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8aa075b9-f0ba-4a5f-ae9d-a9e16876def3",
            "compositeImage": {
                "id": "27f0148a-e594-49fc-9642-7aebd3b92199",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dacd9aa-8c13-4fcf-8497-366cadf9e5f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d5c9556-fa29-468d-b264-d8e79f84027c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dacd9aa-8c13-4fcf-8497-366cadf9e5f4",
                    "LayerId": "8e791faa-c19f-4e54-818e-c35a6b3befeb"
                }
            ]
        },
        {
            "id": "33bd86c6-f1f0-4219-a307-522a7a7fb914",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8aa075b9-f0ba-4a5f-ae9d-a9e16876def3",
            "compositeImage": {
                "id": "9c6f67c1-6d68-4074-ba03-805a8363f589",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33bd86c6-f1f0-4219-a307-522a7a7fb914",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc5b5626-3fd6-4bce-a72e-3e5ce5d936ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33bd86c6-f1f0-4219-a307-522a7a7fb914",
                    "LayerId": "8e791faa-c19f-4e54-818e-c35a6b3befeb"
                }
            ]
        },
        {
            "id": "e86c7976-f5a2-4d12-afc3-d27e9dfcdbcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8aa075b9-f0ba-4a5f-ae9d-a9e16876def3",
            "compositeImage": {
                "id": "77bebee1-3eb2-45f6-9e87-c78807d2bb40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e86c7976-f5a2-4d12-afc3-d27e9dfcdbcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0002244-39d9-42e4-8e84-e7d2ca872980",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e86c7976-f5a2-4d12-afc3-d27e9dfcdbcb",
                    "LayerId": "8e791faa-c19f-4e54-818e-c35a6b3befeb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8e791faa-c19f-4e54-818e-c35a6b3befeb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8aa075b9-f0ba-4a5f-ae9d-a9e16876def3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}