{
    "id": "06651285-9435-4b4d-b6d2-1020999bcf30",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWiggley",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd183bbc-f623-4709-b317-91ed16170726",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06651285-9435-4b4d-b6d2-1020999bcf30",
            "compositeImage": {
                "id": "8efec8d7-1989-4e7e-b9fd-49454e0e44e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd183bbc-f623-4709-b317-91ed16170726",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3ec67bb-c4bb-40f4-971e-a3c920296551",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd183bbc-f623-4709-b317-91ed16170726",
                    "LayerId": "0289c228-46e6-45c6-8280-7af727df6814"
                }
            ]
        },
        {
            "id": "acf6dedc-e2a7-41b8-ac2f-634edc4bb584",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06651285-9435-4b4d-b6d2-1020999bcf30",
            "compositeImage": {
                "id": "fb5f0cfc-2e00-4899-8ff2-7c34541af494",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acf6dedc-e2a7-41b8-ac2f-634edc4bb584",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25ebf134-4ab1-4552-bd2f-82bd25f8bf87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acf6dedc-e2a7-41b8-ac2f-634edc4bb584",
                    "LayerId": "0289c228-46e6-45c6-8280-7af727df6814"
                }
            ]
        },
        {
            "id": "514e00b4-e44d-45ad-b4d3-cce4bd736b8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06651285-9435-4b4d-b6d2-1020999bcf30",
            "compositeImage": {
                "id": "44342720-de1a-4798-bd6c-7c05e37e1036",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "514e00b4-e44d-45ad-b4d3-cce4bd736b8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d657493-ed68-49fd-9f12-006f378ea460",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "514e00b4-e44d-45ad-b4d3-cce4bd736b8b",
                    "LayerId": "0289c228-46e6-45c6-8280-7af727df6814"
                }
            ]
        },
        {
            "id": "0ba60594-7024-4c07-b8be-6f440c6e0f11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06651285-9435-4b4d-b6d2-1020999bcf30",
            "compositeImage": {
                "id": "17b25f53-59f0-4edd-b90a-8f4d95fcf5c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ba60594-7024-4c07-b8be-6f440c6e0f11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb21c284-357a-49f1-958a-019a5361e862",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ba60594-7024-4c07-b8be-6f440c6e0f11",
                    "LayerId": "0289c228-46e6-45c6-8280-7af727df6814"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0289c228-46e6-45c6-8280-7af727df6814",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06651285-9435-4b4d-b6d2-1020999bcf30",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}