{
    "id": "638bb1c6-f884-4fa4-9142-fd2bd2a09609",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGrumpbo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b168070-25e0-4967-8d17-00360e30927f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "638bb1c6-f884-4fa4-9142-fd2bd2a09609",
            "compositeImage": {
                "id": "0ff65ab3-62fb-47e4-b776-01c4f22282af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b168070-25e0-4967-8d17-00360e30927f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b60b751-91fd-489d-a4c6-6def0182443a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b168070-25e0-4967-8d17-00360e30927f",
                    "LayerId": "0b7e7768-e372-4c2f-9b12-647e0e40cfcc"
                }
            ]
        },
        {
            "id": "9de36b74-2d96-4b6f-8d2d-4533beed8409",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "638bb1c6-f884-4fa4-9142-fd2bd2a09609",
            "compositeImage": {
                "id": "8a572e2a-085e-4a73-ba76-1f925f1974df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9de36b74-2d96-4b6f-8d2d-4533beed8409",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "920e4dc6-16fd-4cb7-aad0-4e5e7ac033a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9de36b74-2d96-4b6f-8d2d-4533beed8409",
                    "LayerId": "0b7e7768-e372-4c2f-9b12-647e0e40cfcc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0b7e7768-e372-4c2f-9b12-647e0e40cfcc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "638bb1c6-f884-4fa4-9142-fd2bd2a09609",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}