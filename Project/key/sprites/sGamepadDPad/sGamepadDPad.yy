{
    "id": "58df0905-da6a-4db7-bade-dd9cea2b5e34",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGamepadDPad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 26,
    "bbox_right": 39,
    "bbox_top": 36,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e824bbb-0a12-423a-a433-5ee998a6a626",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58df0905-da6a-4db7-bade-dd9cea2b5e34",
            "compositeImage": {
                "id": "1371f7e9-9460-4195-84b3-3da0ace61d1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e824bbb-0a12-423a-a433-5ee998a6a626",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4290a999-ff8a-4e89-a840-d5d455e58bc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e824bbb-0a12-423a-a433-5ee998a6a626",
                    "LayerId": "f935d668-3082-4685-8b77-697033951124"
                },
                {
                    "id": "9ba65382-c10e-4078-9bc5-81d0f8a4b365",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e824bbb-0a12-423a-a433-5ee998a6a626",
                    "LayerId": "3f904d31-4c39-4ba4-8c9d-e60f07d0e068"
                }
            ]
        },
        {
            "id": "95298fd0-14c0-4c64-af5d-a4daabb9cf2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58df0905-da6a-4db7-bade-dd9cea2b5e34",
            "compositeImage": {
                "id": "22ac60ef-c94d-4ec0-b27a-787f2480e36f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95298fd0-14c0-4c64-af5d-a4daabb9cf2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a8e963f-2303-4326-b04c-9dbb2d656361",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95298fd0-14c0-4c64-af5d-a4daabb9cf2c",
                    "LayerId": "f935d668-3082-4685-8b77-697033951124"
                },
                {
                    "id": "06b63378-eb28-4690-a47f-80ebdc394236",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95298fd0-14c0-4c64-af5d-a4daabb9cf2c",
                    "LayerId": "3f904d31-4c39-4ba4-8c9d-e60f07d0e068"
                }
            ]
        },
        {
            "id": "cbab2f81-9c2b-4eac-8895-5151786811fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58df0905-da6a-4db7-bade-dd9cea2b5e34",
            "compositeImage": {
                "id": "b616ae28-c951-4ada-a131-6050d3ff8862",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbab2f81-9c2b-4eac-8895-5151786811fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d75fd250-0880-49f8-bc3d-0b09e14f367a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbab2f81-9c2b-4eac-8895-5151786811fd",
                    "LayerId": "f935d668-3082-4685-8b77-697033951124"
                },
                {
                    "id": "4b9931da-b087-4ad4-843c-83f0a9e1cd72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbab2f81-9c2b-4eac-8895-5151786811fd",
                    "LayerId": "3f904d31-4c39-4ba4-8c9d-e60f07d0e068"
                }
            ]
        },
        {
            "id": "7001f207-e54d-4f1c-b3bc-0d1ccddb30a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58df0905-da6a-4db7-bade-dd9cea2b5e34",
            "compositeImage": {
                "id": "ad387cb0-f458-4396-8330-4874d6c9ae94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7001f207-e54d-4f1c-b3bc-0d1ccddb30a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa8301a6-0f81-453f-bc4d-f75bbb1e3329",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7001f207-e54d-4f1c-b3bc-0d1ccddb30a3",
                    "LayerId": "f935d668-3082-4685-8b77-697033951124"
                },
                {
                    "id": "d41f2a36-c204-4739-ba57-73fab89ce593",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7001f207-e54d-4f1c-b3bc-0d1ccddb30a3",
                    "LayerId": "3f904d31-4c39-4ba4-8c9d-e60f07d0e068"
                }
            ]
        },
        {
            "id": "1be6378e-ce42-41ae-88ae-f221c6429420",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58df0905-da6a-4db7-bade-dd9cea2b5e34",
            "compositeImage": {
                "id": "8f92c591-953f-4965-a937-44ccf540ec71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1be6378e-ce42-41ae-88ae-f221c6429420",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2383d161-37e6-4b68-ae9d-5ebc4a11d645",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1be6378e-ce42-41ae-88ae-f221c6429420",
                    "LayerId": "f935d668-3082-4685-8b77-697033951124"
                },
                {
                    "id": "e0064c2f-d3e4-43a0-9313-ce6eb656f31d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1be6378e-ce42-41ae-88ae-f221c6429420",
                    "LayerId": "3f904d31-4c39-4ba4-8c9d-e60f07d0e068"
                }
            ]
        },
        {
            "id": "91c7b32c-49ba-4b77-af50-a71ba69f05fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58df0905-da6a-4db7-bade-dd9cea2b5e34",
            "compositeImage": {
                "id": "48fe3d4f-9275-4b8d-a03c-19851216c985",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91c7b32c-49ba-4b77-af50-a71ba69f05fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "545a21e0-c6fa-4569-ae0b-487df1cf8eb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91c7b32c-49ba-4b77-af50-a71ba69f05fc",
                    "LayerId": "f935d668-3082-4685-8b77-697033951124"
                },
                {
                    "id": "04230ce7-ffac-4a3a-8fd5-4f0048950fee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91c7b32c-49ba-4b77-af50-a71ba69f05fc",
                    "LayerId": "3f904d31-4c39-4ba4-8c9d-e60f07d0e068"
                }
            ]
        },
        {
            "id": "e0b97a9a-810c-4123-a46f-bf0e0535b234",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58df0905-da6a-4db7-bade-dd9cea2b5e34",
            "compositeImage": {
                "id": "5c2c2439-32ec-4086-bd23-a89269c3dd89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0b97a9a-810c-4123-a46f-bf0e0535b234",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b68e6b0e-538b-4591-9cde-c58a4b1713aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0b97a9a-810c-4123-a46f-bf0e0535b234",
                    "LayerId": "f935d668-3082-4685-8b77-697033951124"
                },
                {
                    "id": "cb5f37ed-df5f-4eac-88c4-dffb0b2e1f54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0b97a9a-810c-4123-a46f-bf0e0535b234",
                    "LayerId": "3f904d31-4c39-4ba4-8c9d-e60f07d0e068"
                }
            ]
        },
        {
            "id": "3c5574a9-c451-462b-b29c-a6183e3ca241",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58df0905-da6a-4db7-bade-dd9cea2b5e34",
            "compositeImage": {
                "id": "6d5b4ebc-7db3-4b0d-8821-b183da1a1dfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c5574a9-c451-462b-b29c-a6183e3ca241",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e330e994-4e40-46ed-8d37-dee0ed1c9f2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c5574a9-c451-462b-b29c-a6183e3ca241",
                    "LayerId": "f935d668-3082-4685-8b77-697033951124"
                },
                {
                    "id": "8fb46d89-d622-4f8a-848c-1811c43dc206",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c5574a9-c451-462b-b29c-a6183e3ca241",
                    "LayerId": "3f904d31-4c39-4ba4-8c9d-e60f07d0e068"
                }
            ]
        },
        {
            "id": "507a8a6d-ef86-46b9-a9ce-4ad39f734cc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58df0905-da6a-4db7-bade-dd9cea2b5e34",
            "compositeImage": {
                "id": "6201f67a-890e-4894-a530-0af928ed682c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "507a8a6d-ef86-46b9-a9ce-4ad39f734cc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddf37aaf-3ad4-4e13-b154-4d484b63f35a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "507a8a6d-ef86-46b9-a9ce-4ad39f734cc8",
                    "LayerId": "f935d668-3082-4685-8b77-697033951124"
                },
                {
                    "id": "de997576-3e36-4a06-9519-a91144184c13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "507a8a6d-ef86-46b9-a9ce-4ad39f734cc8",
                    "LayerId": "3f904d31-4c39-4ba4-8c9d-e60f07d0e068"
                }
            ]
        },
        {
            "id": "2a26c449-0dc6-46d9-b9a6-256eb9e24e3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58df0905-da6a-4db7-bade-dd9cea2b5e34",
            "compositeImage": {
                "id": "4910992f-8828-44b6-95ac-a840c0327e13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a26c449-0dc6-46d9-b9a6-256eb9e24e3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dda8aa4-e348-4d3a-8b4e-58fadfe9fc07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a26c449-0dc6-46d9-b9a6-256eb9e24e3d",
                    "LayerId": "f935d668-3082-4685-8b77-697033951124"
                },
                {
                    "id": "8d04de42-da27-4372-ba78-92a1bf998d00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a26c449-0dc6-46d9-b9a6-256eb9e24e3d",
                    "LayerId": "3f904d31-4c39-4ba4-8c9d-e60f07d0e068"
                }
            ]
        },
        {
            "id": "65a89e64-4452-4495-b65e-e12823197c5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58df0905-da6a-4db7-bade-dd9cea2b5e34",
            "compositeImage": {
                "id": "5bac01bb-1265-4d5a-a31d-dccfcabf8745",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65a89e64-4452-4495-b65e-e12823197c5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "134b5134-abf4-400a-9653-acd164630bcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65a89e64-4452-4495-b65e-e12823197c5d",
                    "LayerId": "f935d668-3082-4685-8b77-697033951124"
                },
                {
                    "id": "226e76a1-65fe-433d-ac26-edbe5c18f6b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65a89e64-4452-4495-b65e-e12823197c5d",
                    "LayerId": "3f904d31-4c39-4ba4-8c9d-e60f07d0e068"
                }
            ]
        },
        {
            "id": "e0afa069-0ce2-450a-9620-e217676d19b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58df0905-da6a-4db7-bade-dd9cea2b5e34",
            "compositeImage": {
                "id": "41504266-7236-40aa-b467-37cdd5a35e57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0afa069-0ce2-450a-9620-e217676d19b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "799831a5-9e82-490e-9c61-319ecd8b378f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0afa069-0ce2-450a-9620-e217676d19b9",
                    "LayerId": "f935d668-3082-4685-8b77-697033951124"
                },
                {
                    "id": "7cc1eb7f-84c3-4c04-bb66-ee165eda5d4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0afa069-0ce2-450a-9620-e217676d19b9",
                    "LayerId": "3f904d31-4c39-4ba4-8c9d-e60f07d0e068"
                }
            ]
        },
        {
            "id": "2b0f8885-58c4-4ee7-9405-b8e08eb242cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58df0905-da6a-4db7-bade-dd9cea2b5e34",
            "compositeImage": {
                "id": "c1284551-933a-4e4b-afd9-d362d6122499",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b0f8885-58c4-4ee7-9405-b8e08eb242cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c9b37df-a122-4495-b79a-396ee5667223",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b0f8885-58c4-4ee7-9405-b8e08eb242cb",
                    "LayerId": "f935d668-3082-4685-8b77-697033951124"
                },
                {
                    "id": "e60934a5-d337-43cb-b64f-f4223e7ce438",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b0f8885-58c4-4ee7-9405-b8e08eb242cb",
                    "LayerId": "3f904d31-4c39-4ba4-8c9d-e60f07d0e068"
                }
            ]
        },
        {
            "id": "8ecf66f4-c50e-4ee7-970f-3b5578843e08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58df0905-da6a-4db7-bade-dd9cea2b5e34",
            "compositeImage": {
                "id": "aff523cc-8b4c-49c1-8cf2-b6b33b4c1f91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ecf66f4-c50e-4ee7-970f-3b5578843e08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b151e09f-6b2b-4ded-8eac-3263b4f665ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ecf66f4-c50e-4ee7-970f-3b5578843e08",
                    "LayerId": "f935d668-3082-4685-8b77-697033951124"
                },
                {
                    "id": "ea4c5e2f-aa3c-46d8-a37a-5743e7d7530a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ecf66f4-c50e-4ee7-970f-3b5578843e08",
                    "LayerId": "3f904d31-4c39-4ba4-8c9d-e60f07d0e068"
                }
            ]
        },
        {
            "id": "61944a38-d20e-4bca-b479-2e6ecdcc4dca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58df0905-da6a-4db7-bade-dd9cea2b5e34",
            "compositeImage": {
                "id": "4d18145c-56b2-4e99-a102-8a3758723d35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61944a38-d20e-4bca-b479-2e6ecdcc4dca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7003872-ecb2-465b-9c62-6c009ed5d9c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61944a38-d20e-4bca-b479-2e6ecdcc4dca",
                    "LayerId": "f935d668-3082-4685-8b77-697033951124"
                },
                {
                    "id": "970f93b6-ccd4-4e0a-bbae-de0d1178754f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61944a38-d20e-4bca-b479-2e6ecdcc4dca",
                    "LayerId": "3f904d31-4c39-4ba4-8c9d-e60f07d0e068"
                }
            ]
        },
        {
            "id": "1e6ca79e-ee43-493a-8cf9-7d81cb9926af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58df0905-da6a-4db7-bade-dd9cea2b5e34",
            "compositeImage": {
                "id": "60e5dc5a-0a52-45ff-b5fa-1c0947caa40e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e6ca79e-ee43-493a-8cf9-7d81cb9926af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6794901e-d660-43ff-b26c-50a82003cfd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e6ca79e-ee43-493a-8cf9-7d81cb9926af",
                    "LayerId": "f935d668-3082-4685-8b77-697033951124"
                },
                {
                    "id": "54fc47d1-baa4-414c-a71e-7fcdcb7d7139",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e6ca79e-ee43-493a-8cf9-7d81cb9926af",
                    "LayerId": "3f904d31-4c39-4ba4-8c9d-e60f07d0e068"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 64,
    "layers": [
        {
            "id": "f935d668-3082-4685-8b77-697033951124",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58df0905-da6a-4db7-bade-dd9cea2b5e34",
            "blendMode": 0,
            "isLocked": false,
            "name": "buttons",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "3f904d31-4c39-4ba4-8c9d-e60f07d0e068",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58df0905-da6a-4db7-bade-dd9cea2b5e34",
            "blendMode": 0,
            "isLocked": false,
            "name": "pad",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 32
}