{
    "id": "ba8f15e9-ce18-4bcd-9d39-a74afec01ac3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFlower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 4,
    "bbox_right": 12,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0b41a16-05bc-4868-b1f2-6adf1d365718",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba8f15e9-ce18-4bcd-9d39-a74afec01ac3",
            "compositeImage": {
                "id": "da5d02de-7e6d-48b3-8fed-5ee49f86b48d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0b41a16-05bc-4868-b1f2-6adf1d365718",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d9e1e13-ef35-48a2-bf71-1cb71ebe6386",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0b41a16-05bc-4868-b1f2-6adf1d365718",
                    "LayerId": "5dd61ac2-172b-4d76-8d96-62eba2da2d97"
                }
            ]
        },
        {
            "id": "94e691ce-959b-498b-ba05-91dded31838d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba8f15e9-ce18-4bcd-9d39-a74afec01ac3",
            "compositeImage": {
                "id": "22288ce1-daf7-4a14-94f4-35954f2c44d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94e691ce-959b-498b-ba05-91dded31838d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cddc6ec7-ede4-4798-a18e-3f19ba0efe43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94e691ce-959b-498b-ba05-91dded31838d",
                    "LayerId": "5dd61ac2-172b-4d76-8d96-62eba2da2d97"
                }
            ]
        },
        {
            "id": "dde12c54-c7b9-4eb9-bb28-f1a3b827f29f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba8f15e9-ce18-4bcd-9d39-a74afec01ac3",
            "compositeImage": {
                "id": "058b9b5d-2dc5-4955-bdfc-2ad28f7e94c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dde12c54-c7b9-4eb9-bb28-f1a3b827f29f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93215545-06bb-4c00-b3d1-4321574e4e7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dde12c54-c7b9-4eb9-bb28-f1a3b827f29f",
                    "LayerId": "5dd61ac2-172b-4d76-8d96-62eba2da2d97"
                }
            ]
        },
        {
            "id": "21ce3d0e-441a-4a58-9d24-1ae46b787347",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba8f15e9-ce18-4bcd-9d39-a74afec01ac3",
            "compositeImage": {
                "id": "fb606bce-f811-4f18-a803-296f1ea2e7b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21ce3d0e-441a-4a58-9d24-1ae46b787347",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a91e9f5-4018-4f6e-aa8e-3aab60c26186",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21ce3d0e-441a-4a58-9d24-1ae46b787347",
                    "LayerId": "5dd61ac2-172b-4d76-8d96-62eba2da2d97"
                }
            ]
        },
        {
            "id": "8405a9be-91d0-4c7f-8c3f-8054662294f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba8f15e9-ce18-4bcd-9d39-a74afec01ac3",
            "compositeImage": {
                "id": "4123a89d-6083-476c-8101-e071a1f78c5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8405a9be-91d0-4c7f-8c3f-8054662294f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6195fba8-1cbe-4a88-9559-d08cf69b301d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8405a9be-91d0-4c7f-8c3f-8054662294f7",
                    "LayerId": "5dd61ac2-172b-4d76-8d96-62eba2da2d97"
                }
            ]
        },
        {
            "id": "3d981284-1136-4608-8763-44ccfd67128a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba8f15e9-ce18-4bcd-9d39-a74afec01ac3",
            "compositeImage": {
                "id": "76e12aa9-f4a4-4d40-8627-a3aa725eeb50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d981284-1136-4608-8763-44ccfd67128a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09fe731c-d689-4985-8edd-be597c686639",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d981284-1136-4608-8763-44ccfd67128a",
                    "LayerId": "5dd61ac2-172b-4d76-8d96-62eba2da2d97"
                }
            ]
        },
        {
            "id": "f2a280b4-c63f-4691-af0a-215c8a6ae3cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba8f15e9-ce18-4bcd-9d39-a74afec01ac3",
            "compositeImage": {
                "id": "ecc53236-b407-4fe8-9b52-12f5e4c71b88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2a280b4-c63f-4691-af0a-215c8a6ae3cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2ba4517-9e0b-4529-9e67-a163a0904ce4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2a280b4-c63f-4691-af0a-215c8a6ae3cd",
                    "LayerId": "5dd61ac2-172b-4d76-8d96-62eba2da2d97"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5dd61ac2-172b-4d76-8d96-62eba2da2d97",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba8f15e9-ce18-4bcd-9d39-a74afec01ac3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}