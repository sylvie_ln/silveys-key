{
    "id": "cb1759c3-681b-447f-93aa-0928f0bba37c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSatan",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13d347ac-a6e4-40af-a59d-9f88cecda52f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb1759c3-681b-447f-93aa-0928f0bba37c",
            "compositeImage": {
                "id": "d13cc507-4335-4a38-aa0f-ee71f29dada9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13d347ac-a6e4-40af-a59d-9f88cecda52f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3945f093-4be6-4f54-bc2f-7840666db4cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13d347ac-a6e4-40af-a59d-9f88cecda52f",
                    "LayerId": "abd6f02f-0a78-4c33-86f2-34eb1848b103"
                }
            ]
        },
        {
            "id": "4912c2fc-8ba8-4311-9c50-ce2d803ef804",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb1759c3-681b-447f-93aa-0928f0bba37c",
            "compositeImage": {
                "id": "0c3dc598-afb9-454d-82ee-1591da63680a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4912c2fc-8ba8-4311-9c50-ce2d803ef804",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53a37bfa-5df1-4491-8114-17b5c254607f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4912c2fc-8ba8-4311-9c50-ce2d803ef804",
                    "LayerId": "abd6f02f-0a78-4c33-86f2-34eb1848b103"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "abd6f02f-0a78-4c33-86f2-34eb1848b103",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb1759c3-681b-447f-93aa-0928f0bba37c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}