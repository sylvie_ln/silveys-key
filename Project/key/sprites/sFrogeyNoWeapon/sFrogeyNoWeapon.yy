{
    "id": "c85e6e8d-5291-4970-82c4-1da1d34eb3e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFrogeyNoWeapon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff0f1221-a358-46c3-8aa9-356b9f94b123",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c85e6e8d-5291-4970-82c4-1da1d34eb3e7",
            "compositeImage": {
                "id": "f51ffe7d-b260-4ca2-8f1c-e3e44b691c0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff0f1221-a358-46c3-8aa9-356b9f94b123",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "433d8070-a99b-4c95-8c6f-24de95ec02c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff0f1221-a358-46c3-8aa9-356b9f94b123",
                    "LayerId": "d84121e3-9173-444c-9f80-7a3def250aca"
                }
            ]
        },
        {
            "id": "64546f8a-5657-444d-91be-a015aac4aa7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c85e6e8d-5291-4970-82c4-1da1d34eb3e7",
            "compositeImage": {
                "id": "fb18f303-fe61-4be7-9b5f-3e2954c9664c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64546f8a-5657-444d-91be-a015aac4aa7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8daab6e-d26e-453e-9649-800be20d959c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64546f8a-5657-444d-91be-a015aac4aa7e",
                    "LayerId": "d84121e3-9173-444c-9f80-7a3def250aca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d84121e3-9173-444c-9f80-7a3def250aca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c85e6e8d-5291-4970-82c4-1da1d34eb3e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}