{
    "id": "2499c9a6-ae84-4e96-841c-6be71c10c9c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sJumpOverMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7f5fcdb-2c93-4550-bcb3-aacf6e5733c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2499c9a6-ae84-4e96-841c-6be71c10c9c5",
            "compositeImage": {
                "id": "790aa01d-58fc-4af0-9b59-5e0e72a85c89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7f5fcdb-2c93-4550-bcb3-aacf6e5733c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb0258f4-7f79-4a20-8a26-6cc690ca9bbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7f5fcdb-2c93-4550-bcb3-aacf6e5733c4",
                    "LayerId": "3bfd9284-6dab-4193-ad85-86a281732aab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3bfd9284-6dab-4193-ad85-86a281732aab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2499c9a6-ae84-4e96-841c-6be71c10c9c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}