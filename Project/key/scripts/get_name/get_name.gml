var t;
switch(argument[0]) {
	case 1:
		t = "Let's Go!"; break;
	case 2:
		t = "Ancient Pyramid"; break;
	case 3:
		t = "Babis Hangout"; break;
	case 6:
		t = "Underground Maze"; break;
	case 7:
		t = "Getting Lost"; break;
	case 10:
		t = "Fire and Flames"; break;
	case 11:
		t = "Secret of the Fellows"; break;
	case 13:
		t = "Gridley Forest"; break;
	case 19:
		t = "The Prison"; break;
	case 24:
		t = "Falling Down!"; break;
	case 34:
		t = "Big Blocks World"; break;
	case 45:
		t = "Hurry! Hurry!"; break;
	case 55:
	case 56:
	case 57:
	case 58:
	case 59:
	case 60:
	case 61:
	case 62:
		t = "Satin's Lair"; break;
	case 73:
		t = "Cute!"; break;
	case 95:
		t = "Key Museum"; break;
	case 100:
		t = "Kittey Paradise!" break;
	default:
		t = "Chess Area"; break;
}
return t;