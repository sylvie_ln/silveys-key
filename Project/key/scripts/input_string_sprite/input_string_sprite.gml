var input = argument[0];
var kind = input[|0];
switch(kind) {
	case input_kind.key:
		var key = input[|1];
		return input_key_string(key);
	case input_kind.gamepad_button:
		var button = input[|1];    
		switch(button) {
			case gp_face1:
		        return "\\sConfigFaceButtons:2";
		    case gp_face2:
		        return "\\sConfigFaceButtons:8";
		    case gp_face3:
		        return "\\sConfigFaceButtons:4";
		    case gp_face4:
		        return "\\sConfigFaceButtons:1";
		    case gp_padd:
		        return "\\sConfigDPad:1";
		    case gp_padl:
		        return "\\sConfigDPad:2";
		    case gp_padr:
		        return "\\sConfigDPad:3";
		    case gp_padu:
		        return "\\sConfigDPad:0";
		    case gp_shoulderl:
		        return "L Front";
		    case gp_shoulderlb:
		        return "L Back";
		    case gp_shoulderr:
		        return "R Front";
		    case gp_shoulderrb:
		        return "R Back";
		    case gp_start:
		        return "Start";
		    case gp_select:
		        return "Select";
		    case gp_stickl:
				return "\\sConfigLStick:0 Press";
		    case gp_stickr:
				return "\\sConfigRStick:0 Press";
		    default:
		        return "Unknown";
		}
	case input_kind.gamepad_axis:
		var axis = input[|1];
		var dir = input[|2];
		switch(axis) {
			case gp_axislh:	
				return (dir == 1) ? "\\sConfigLStick:0 Right" : "\\sConfigLStick:0 Left";
			case gp_axisrh:	
				return (dir == 1) ? "\\sConfigRStick:0 Right" : "\\sConfigRStick:0 Left";
			case gp_axislv:	
				return (dir == 1) ? "\\sConfigLStick:0 Down" : "\\sConfigLStick:0 Up";
			case gp_axisrv:	
				return (dir == 1) ? "\\sConfigRStick:0 Down" : "\\sConfigRStick:0 Up";
		    default:
		        return "Unknown";
		}
	break;
	case input_kind.gamepad_hat:
		var dir = input[|2];
		return "\\sConfigHat:"+string(dir);
	break;
	case input_kind.mouse:	
		var button = input[|1];
		switch(button) {
			case mb_left: 
				return "Left Click";
			case mb_right: 
				return "Right Click";
			case mb_middle: 
				return "Middle Click";
		}
	break;
	case input_kind.waiting:
		return "(Press Button)";
	break;
}