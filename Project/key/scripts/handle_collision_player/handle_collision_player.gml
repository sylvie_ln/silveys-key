var list = argument[0];
var stop = false;
for(var i=0; i<ds_list_size(list); i++) {
	var inst = list[|i];
	var obj = inst.object_index;
	if inst.solid {
		stop = true;	
	}
	if object_is_ancestor(obj,oSilveyBlock) or obj == oSilveyBlock {
		with inst {
			if other.bbox_top > bbox_bottom {
				event_user(0); // butt
			}
		}
	}
	if object_is_ancestor(obj,oItem) {
		with inst {
			if !hidden {
				event_user(0);
			}
		}
	}
}
ds_list_destroy(list);
return stop;