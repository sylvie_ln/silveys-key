///@description Set which gamepad to use when reading gamepad input for this instance.
///@param gamepad The gamepad id to use when reading gamepad input for this instance.
ds_map_add(oInput.gpad_map,id,argument[0]);