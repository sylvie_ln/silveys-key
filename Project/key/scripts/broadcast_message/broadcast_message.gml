with all {
	if variable_instance_exists(id,"messages") {
		var m = array_create(argument_count);
		for(var i=0; i<argument_count; i++) {
			m[i] = argument[i];	
		}
		if ds_exists(messages,ds_type_queue) {
			ds_queue_enqueue(messages,m);
		}
	}
}