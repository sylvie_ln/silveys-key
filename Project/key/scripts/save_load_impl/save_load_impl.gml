var map = argument[1];
var type = argument[2]

if argument[0] == "save" {
	global_script = set_map_global;
	key_script = save_keys;
}
if argument[0] == "load" {
	global_script = get_map_global;
	key_script = load_keys;
}

if type == "game" {
	for(var i=0; i<=100; i++) {
		variable_global_set("visited_"+string(i),global.visited[i]);
		script_execute(global_script,"visited_"+string(i),map);
		global.visited[i] = variable_global_get("visited_"+string(i));
	}
} else if type == "options" {
	script_execute(global_script,"scale",map);
	script_execute(global_script,"volume",map);
	script_execute(key_script,map);
}

return map;