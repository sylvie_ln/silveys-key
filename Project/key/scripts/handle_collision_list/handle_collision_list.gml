var list = argument[0];
var stop = false;
for(var i=0; i<ds_list_size(list); i++) {
	var inst = list[|i];
	if inst.solid {
		stop = true;	
	}
}
ds_list_destroy(list);
return stop;