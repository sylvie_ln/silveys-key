var axes = [gp_axislh,gp_axisrh,gp_axislv,gp_axisrv];
var threshold = 0.5;
for(var g=0; g<gamepad_get_device_count(); g++) {
	if !gamepad_is_connected(g) { continue; }
	for(var i=0; i<array_length_1d(axes); i++) {
		var axis = axes[i];
		var avalue = gamepad_axis_value(g,axis);
		var pvalue = oInput.previous_axis_map[? string(g)+":"+string(axis)];
		var asign = sign(avalue); var psign = sign(pvalue);
		if (abs(avalue) >= threshold and abs(pvalue) < threshold) {
			return [axis,asign];	
		}
	}
}
return [-1,-1];